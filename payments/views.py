from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext as _
from django.views import View
from django.views.generic.detail import SingleObjectMixin
from rules.contrib.views import (permission_required,
                                 objectgetter,
                                 PermissionRequiredMixin)

from accommodations.models import AccommodationReservation
from include.mails import MailBuilder
from payments.forms import PaymentForm
from payments.models import Payment
from teams.models import Team

from .processor import (collect_payment,
                        create_team_payment, Receipt,
                        create_team_payment_link,
                        is_team_payment_paid, mark_team_paid,
                        remove_team_payment)
from .tables import PaymentTable
from .consts import GATEWAY_PAYMENT


class DisplayTeamPayment(SingleObjectMixin, PermissionRequiredMixin, View):
    model = Team
    permission_required = 'payments.add_teampayment'

    def get(self, request, pk):
        team = self.get_object()
        if not team.game_rules.tournament.is_registration_active():
            messages.error(request, _('You are unable to pay for team in'
                                      ' inactive tournament'))
            return redirect('teams_detail', team.pk)
        try:
            payment = Payment.objects.get(transaction_id=team.get_vs())
            if is_team_payment_paid(payment):
                mark_team_paid(team)
                payment = Payment.objects.get(transaction_id=team.get_vs())
            payment_lst = Receipt(payment.paymentitem_set.all())
        except ObjectDoesNotExist:
            AccommodationReservation.objects.filter_by_emails(
                team.get_emails(), paymentitem__isnull=True).update(team=team)
            payment_lst = collect_payment(team, paymentitem__isnull=True)

        table = PaymentTable(payment_lst)

        profile = request.user.profile

        initial = {
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'email': request.user.email,
            'country': profile.country,
            'state': profile.state,
            'city': profile.city,
            'street': profile.street,
            'zip_code': profile.zip_code
        }

        form = PaymentForm(initial=initial)

        paid_qs = team.paymentitem_set.filter(payment__pay_date__isnull=False)
        paid = paid_qs.exists()

        context = {'table': table,
                   'overall': payment_lst.sum_price(),
                   'is_not_paid': not paid,  # TODO treba upravit na is_paid
                   'team': team,
                   'form': form}

        return render(request, 'payments/payment.html', context=context)

    def post(self, request, pk):
        team = self.get_object()

        if team.paymentitem_set.filter(payment__pay_date__isnull=False):
            messages.error(request, _('You can not pay for team that has'
                                      ' already everything paid'))
            return redirect('team_payment', team.pk)

        if not team.game_rules.tournament.is_registration_active():
            messages.error(request, _('Unable to pay for team created in'
                                      ' inactive tournament'))
            return redirect('teams_detail', team.pk)

        form = PaymentForm(request.POST)

        if not form.is_valid():
            messages.error(request, _('Payment has not valid data'))
            return redirect('team_payment', team.pk)

        self._update_user_profile_contact_info(request.user.profile, form)

        try:
            payment = Payment.objects.get(transaction_id=team.get_vs())
            payment_lst = Receipt(payment.paymentitem_set.all())
            table = PaymentTable(payment_lst)
            context = {'team': team,
                       'overall': payment_lst.sum_price(),
                       'table': table,
                       'items': payment_lst,
                       'is_not_paid': payment.pay_date is None}
        except ObjectDoesNotExist:
            payment = create_team_payment(team, request.user)
            payment_lst = Receipt(payment.paymentitem_set.all())
            table = PaymentTable(payment_lst)
            context = {
                'team': team,
                'overall': payment_lst.sum_price(),
                'table': table,
                'items': payment_lst,
                'is_not_paid': True,
                'admin_name': ', '.join([a.first_name
                                         for a in payment.team.admins.all()])
            }

            builder = MailBuilder(
                to=request.user.email,
                subject=_('[Y-Games] Platba za tím %s') % team.name,
                text_link='email/payment_email.txt',
            )
            builder.auto_generated = True

            email = builder.create(context)
            email.send()

        payment.method = GATEWAY_PAYMENT
        payment.save()

        messages.success(request, _('Payment created and invoice send to'
                                    ' your mailbox'))

#         if method == GATEWAY_PAYMENT:
        return HttpResponseRedirect(create_team_payment_link(payment))
#         else:
#             context.update({'form': form})
#             return render(request, 'payments/payment.html', context=context)

    @staticmethod
    def _update_user_profile_contact_info(profile, payment_form):
        attrs = ['country', 'city', 'state', 'street', 'zip_code']
        DisplayTeamPayment._update_model(profile, payment_form, attrs)

    @staticmethod
    def _update_user(user, payment_form):
        attrs = ['first_name', 'laste_name', 'email']
        DisplayTeamPayment._update_model(user, payment_form, attrs)

    @staticmethod
    def _update_model(model, form, attrs):
        def _update_if_changed(attr, model, form):
            if getattr(model, attr) != form.cleaned_data[attr]:
                setattr(model, attr, form.cleaned_data[attr])
                return True
            else:
                return False

        if any([_update_if_changed(attr, model, form)
                for attr in attrs]):
            model.save()


@permission_required('payments.delete_teampayment', fn=objectgetter(Team))
def remove_payment(request, pk):
    team = get_object_or_404(Team, pk=pk)

    try:
        payment = Payment.objects.get(team=team)
    except ObjectDoesNotExist:
        messages.error(request, _('You don\'t have created any payment.'))
        return redirect('team_payment', team.pk)

    if payment.is_paid():
        messages.error(request, _('You can not delete team payment,'
                                  ' your payment is already paid.'))
        return redirect('team_payment', team.pk)

    remove_team_payment(team)
    messages.success(request, _('Payment successfully removed'))
    return redirect('team_payment', team.pk)

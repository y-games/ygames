import logging
import urllib.parse as urlparse
import requests

from urllib.parse import urlencode

from django.db.models import Q
from django.utils.translation import ugettext as _

from include.mails import MailBuilder
from payments.models import Payment, PaymentItem

logger = logging.getLogger('payments.processor')


class Receipt:
    _DEFAULTS = ('name', 'price', 'quantity', 'ticket_id',
                 'team', 'accommodation', 'product')

    class Product:
        pass

    def __init__(self, items=None):
        def _getter(item):
            return {k: item.__dict__[k] for k in type(self)._DEFAULTS
                    if k in item.__dict__}
        self._items = [_getter(item) for item in items] if items else []

    def add(self, name, price, quantity, ticket_id=None, **kwargs):
        item = {
            'name': name,
            'price': price,
            'quantity': quantity,
            'ticket_id': ticket_id,
        }
        item.update(**kwargs)
        self._items.append(item)

    def add_team(self, team):
        """Adds team payment to receipt.

        Args:
            team (ygames.teams.models.Team): Team instance
        """
        game_rule = team.game_rules
        if game_rule.vip:
            self._add_team(team, '[VIP] Team: {} - {}'.format(team.name,
                                                              game_rule.name),
                           game_rule.price, 1)
        else:
            self._add_team(team, 'Team: {} - {}'.format(team.name,
                                                        game_rule.name),
                           game_rule.price_per_player, game_rule.player_count)

    def _add_team(self, team, name, price, quantity):
        game_rules = team.game_rules
        self.add(name, price, quantity, ticket_id=game_rules.ticket_id,
                 team=team)

    def add_accommodation(self, acc_res):
        acc = acc_res.accommodation
        self.add(str(acc_res), acc.price, 1, ticket_id=acc.ticket_id,
                 accommodation=acc_res)

    def add_product(self, prod_res):
        prod = prod_res.product
        self.add(str(prod_res), prod.price, prod_res.quantity,
                 ticket_id=prod.ticket_id, product=prod_res)

    def to_query(self):
        coll = {}
        for item in self._items:
            coll[item['ticket_id']] = (coll.get(item['ticket_id'], 0) +
                                       item['quantity'])
        return ';'.join(('{}|{}'.format(k, v) for k, v in coll.items()))

    def as_objects(self, cls=Product):
        def _create(item):
            inst = cls()
            inst.__dict__.update(item)
            return inst
        return [_create(item) for item in self._items]

    def as_list(self):
        return list(self)

    def __iter__(self):
        return (dict(item) for item in self._items)

    def sum_price(self):
        return sum((item['price'] * item['quantity'] for item in self._items))


def collect_payment(team, *acc_query, **acc_filter):
    """

    Args:
        acc_state (str|list of str, optional): Accommodation states that are
         collected
        team (ygames.teams.models.Team): Team.

    Returns:
        Receipt: Complete recipe of collected items.
    """
    payment = Receipt()
    payment.add_team(team)

    for acc_res in team.accommodationreservation_set.filter(*acc_query,
                                                            **acc_filter):
        payment.add_accommodation(acc_res)
        for prod_res in acc_res.productreservation_set.all():
            payment.add_product(prod_res)

    return payment


def send_payment_mail(team, to=None, items=None):
    to_ = to or [admin.email for admin in team.admins.all()]
    receipt = items or collect_payment(
        team, (Q(paymentitem__isnull=True) |
               Q(paymentitem__isnull=False,
                 paymentitem__payment__pay_date__isnull=True)))
    items_ = receipt.as_list()
    mail_builder = MailBuilder(
        to=to_,
        subject=_('[Y-Games] Payment for team %(team)s') % {'team': team.name},
        text_link='email/payment_email.txt',
    )
    mail_builder.auto_generated = True
    mail = mail_builder.create({
        'items': items_,
        'team': team,
        'overall': receipt.sum_price()
    })
    mail.send()


def send_payment_received(team, to=None):
    to_ = to or [a.email for a in team.admins.all()]
    mail_builder = MailBuilder(
        to=to_,
        subject=(_('[Y-Games] Payment for team %(team)s received')
                 % {'team': team.name}),
        text_link='email/payment_received.txt'
    )
    mail_builder.auto_generated = True
    mail = mail_builder.create({
        'team': team
    })
    mail.send()


def create_team_payment(team, payer):
    """Creates payment object and locks updates.

    Args:
        payer (django.contrib.auth.models.User): Admin who is paying for team.
        team (ygames.teams.models.Team): Team object.

    Returns:

    """
    if Payment.objects.filter(team=team).exists():
        raise ValueError('Payment for team already exists')
    recipe = collect_payment(team, paymentitem__isnull=True)
    payment = Payment.objects.create(team=team, payer=payer,
                                     transaction_id=team.get_vs())
    PaymentItem.objects.create_from_recipe(payment, recipe)
    return payment


def mark_team_paid(team):
    Payment.objects.pay_for_teams(team)


def mark_team_unpaid(team):
    Payment.objects.unpay_teams(team)


def remove_team_payment(team):
    Payment.objects.filter(team=team, pay_date__isnull=True).delete()


def create_team_payment_link(payment):
    """Creates payment gateway redirection link.

    Args:
        payment (ygames.payments.models.Payment):

    Returns:
        str: Redirect link to payment gate.
    """
    receipt = Receipt(payment.paymentitem_set.all())
    payer = payment.payer
    q = {
        'email': payer.email,
        'name': '{} {}'.format(payer.first_name, payer.last_name),
        'note': payment.team.name,
        'country': payer.profile.country.name,
        'phone': payer.profile.phone_number,
        'city': payer.profile.city,
        'zip': payer.profile.zip_code,
        'street': payer.profile.street,
        'transactionId': payment.transaction_id,
        'tickets': receipt.to_query()
    }
    tournament = payment.team.game_rules.tournament
    url_parts = list(urlparse.urlparse(tournament.gateway_url))
    url_parts[4] = urlencode({k: v for k, v in q.items() if v})
    return urlparse.urlunparse(url_parts)


class GatewayError(Exception):
    pass


def is_team_payment_paid(payment):
    t = payment.team.game_rules.tournament
    if not t.gateway_url:
        logging.error('Tournament {} has no gateway'.format(t.gateway_url))
        return
    url_parts = [p for p in urlparse.urlparse(t.gateway_url)]
    url_parts[2] = '/administration/orders/OrdersCheck.aspx'
    params = {
        'transactionId': payment.transaction_id,
        'email': t.event_admin_email
    }
    response = requests.post(urlparse.urlunparse(url_parts), params=params)

    try:
        response.raise_for_status()
    except Exception as e:
        logger.exception(e)
        raise GatewayError(e)

    content_str = response.content.decode('utf-8')

    if content_str.startswith('ERROR:'):
        raise GatewayError(content_str[len('ERROR:'):])

    try:
        respson = response.json()

        return bool(respson['platba'])
    except Exception as e:
        logger.exception(e)
        return False

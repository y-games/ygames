from django import forms
from django.utils.translation import ugettext as _

from django_countries.fields import LazyTypedChoiceField
from django_countries.widgets import CountrySelectWidget
from django_countries import countries


class PaymentForm(forms.Form):
    first_name = forms.CharField(max_length=32, required=True,
                                 widget=forms.widgets.TextInput(
                                     attrs={'class': 'form-control'}),
                                 label=_('First name'))
    last_name = forms.CharField(max_length=32, required=True,
                                widget=forms.widgets.TextInput(
                                    attrs={'class': 'form-control'}),
                                label=_('Last name'))
    email = forms.EmailField(max_length=32, required=True,
                             widget=forms.EmailInput(
                                attrs={'class': 'form-control'}),
                             label=_('Email'))
    country = LazyTypedChoiceField(choices=countries,
                                   widget=CountrySelectWidget(
                                       attrs={'class': 'form-control'}),
                                   label=_('Country'))
    state = forms.CharField(max_length=100, required=True, label=_('Region'),
                            widget=forms.widgets.TextInput(attrs={
                                'ajax-url': '/territories/load/',
                                'class': 'form-control'
                            }))
    city = forms.CharField(max_length=100, required=True, label=_('City'),
                           widget=forms.widgets.TextInput(
                               attrs={'class': 'form-control'}))
    street = forms.CharField(max_length=100, required=True, label=_('Street'),
                             help_text=_('If your city/village doesn\'t have'
                                         ' streets just type city/village'
                                         ' name again'),
                             widget=forms.widgets.TextInput(
                                 attrs={'class': 'form-control'}))
    zip_code = forms.CharField(max_length=100, required=True, label=_('ZIP'),
                               widget=forms.widgets.TextInput(
                                   attrs={'class': 'form-control'}))

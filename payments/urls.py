from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^team/(?P<pk>\d+)/$', views.DisplayTeamPayment.as_view(),
        name='team_payment'),
    url(r'^team/(?P<pk>\d+)/delete/$', views.remove_payment,
        name='delete_payment')
]

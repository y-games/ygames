import rules


@rules.predicate
def is_team_admin(user, team):
    """

    Args:
        user (django.contrib.auth.models.User): Request user.
        team (ygames.teams.model.Team): Team.

    Returns:
        bool: True If user is team admin.
    """
    if team is None:
        return False
    return team.admins.filter(pk=user.pk).exists()


rules.add_perm('payments.add_teampayment', is_team_admin)
rules.add_perm('payments.change_teampayment', rules.is_staff)
rules.add_perm('payments.delete_teampayment', is_team_admin)

from daterange_filter.filter import DateRangeFilter
from django.contrib import admin

# Register your models here.
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _

from .models import (PaymentItem,
                     Payment)


class PaymentItemInline(admin.StackedInline):
    model = PaymentItem
    fk_name = 'payment'
    extra = 0


class PaymentFilter(admin.SimpleListFilter):
    title = _('paid')
    parameter_name = 'paid'

    def lookups(self, request, model_admin):
        return [
            ('p', _('Paid')),
            ('c', _('Payment created'))
        ]

    def queryset(self, request, queryset):
        if self.value() == 'p':
            return queryset.filter(pay_date__isnull=False)
        if self.value() == 'c':
            return queryset.filter(pay_date__isnull=True)
        return queryset


class PaymentAdmin(admin.ModelAdmin):
    list_display = ('payer', 'team_link', 'transaction_id', 'team_price',
                    'is_paid')
    list_filter = ('team__game_rules__tournament', PaymentFilter,
                   ('team__created', DateRangeFilter), 'method')
    search_fields = ('payer__username', 'payer__email', 'payer__first_name',
                     'payer__last_name', 'team__name', 'team__acronym')

    inlines = [
        PaymentItemInline
    ]

    def team_price(self, obj):
        return '{0:.2f} €'.format(float(obj.price())/100)
    team_price.short_description = _('Amount')

    def team_link(self, obj):
        return mark_safe('<a href="{}">{}</a>'.format(
            reverse('admin:teams_team_change', args=(obj.team.pk,)),
            str(obj.team)
        ))
    team_link.short_description = _('Team')


admin.site.register(Payment, PaymentAdmin)

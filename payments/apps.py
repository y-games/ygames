from django.apps import AppConfig


class PaimentprocessorConfig(AppConfig):
    name = 'payments'

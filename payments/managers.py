from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from accommodations.models import (AccommodationReservation,
                                   ProductReservation)
from teams.models import Team

from .consts import (TEAM_CODE,
                     ACCOMMODATION_CODE,
                     PRODUCT_CODE)

_MAPPING = (
    (TEAM_CODE, Team),
    (ACCOMMODATION_CODE, AccommodationReservation),
    (PRODUCT_CODE, ProductReservation)
)

_NAME_TYPE_MAP = {
    Team: 'team',
    AccommodationReservation: 'accommodation',
    ProductReservation: 'product'
}


class UnknowTypeException(Exception):
    pass


class PaymentManager(models.Manager):

    def create_team_payment(self, payer, team):
        return self.create(payer=payer, team=team,
                           transaction_id=team.get_vs())

    def team_payment_exists(self, team):
        return self.filter(transaction_id=team.get_vs()).exists()

    def pay_for_teams(self, team, *teams):
        if not teams:
            qs = self.filter(team=team, pay_date__isnull=True)
        else:
            teams = [team] + list(teams)
            qs = self.filter(team__in=teams, pay_date__isnull=True)
        return qs.update(pay_date=datetime.now())

    def unpay_teams(self, team, *teams):
        if not teams:
            qs = self.filter(team=team, pay_date__isnull=False)
        else:
            teams = [team] + list(teams)
            qs = self.filter(team__in=teams, pay_date__isnull=False)
        return qs.update(pay_date=None)


class PaymentItemManeger(models.Manager):

    def create_from_recipe(self, payment, recipe):
        instances = [self.model(payment=payment, **item) for item in recipe]
        return self.bulk_create(instances)

    def filter_by_billable(self, billable):
        return self.filter(**{_NAME_TYPE_MAP[type(billable)]: billable})

    def is_paid(self, billable):
        try:
            item = self.filter_by_billable(billable).get()
            return item.payment.is_paid()
        except ObjectDoesNotExist:
            return False

    def is_created(self, billable):
        return self.filter_by_billable(billable).exists()

    def get_from_billable(self, instance):
        return self.filter_by_billable(instance).get()

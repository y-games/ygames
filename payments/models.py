from django.db import models
from django.utils.translation import ugettext_lazy as _

from .consts import (ACCOMMODATION_CODE,
                     GATEWAY_PAYMENT,
                     BANK_TRANSFER,
                     PRODUCT_CODE,
                     TEAM_CODE)

from .managers import (PaymentItemManeger,
                       PaymentManager)


class Payment(models.Model):

    _CHOICES = (
        (GATEWAY_PAYMENT, _('Payment gateway')),
        (BANK_TRANSFER, _('Bank transfer'))
    )

    payer = models.ForeignKey('auth.User')
    team = models.ForeignKey('teams.Team', null=True)
    transaction_id = models.CharField(max_length=16, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    method = models.CharField(max_length=1, default=GATEWAY_PAYMENT,
                              choices=_CHOICES)
    error = models.TextField(blank=True)
    pay_date = models.DateTimeField(null=True)

    objects = PaymentManager()

    def price(self):
        res = PaymentItem.objects.filter(payment=self).aggregate(
            price=models.Sum(models.F('price') * models.F('quantity'),
                             output_field=models.IntegerField()),
        )
        return res['price']

    def is_paid(self):
        return self.pay_date is not None

    is_paid.boolean = True


class PaymentItem(models.Model):

    _CHOICES = (
        (ACCOMMODATION_CODE, _('Accommodation')),
        (PRODUCT_CODE, _('Product')),
        (TEAM_CODE, _('Team'))
    )

    name = models.CharField(max_length=254)
    price = models.IntegerField()
    payment = models.ForeignKey(Payment)
    quantity = models.IntegerField(default=1)
    ticket_id = models.IntegerField()
    team = models.ForeignKey('teams.Team', null=True)
    accommodation = models.ForeignKey(
        'accommodations.AccommodationReservation', null=True)
    product = models.ForeignKey('accommodations.ProductReservation',
                                null=True)

    objects = PaymentItemManeger()

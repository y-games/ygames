from datetime import (datetime,
                      timedelta)
from django.core.management import BaseCommand

from game.models import Tournament

from payments.consts import GATEWAY_PAYMENT
from payments.models import Payment


class Command(BaseCommand):
    help = 'Removes old unpaid registrations'

    def handle(self, *args, **options):
        tourns = Tournament.objects.filter_enabled_registration()

        Payment.objects.filter(
            pay_date__isnull=True,
            team__game_rules__tournament__in=tourns,
            method=GATEWAY_PAYMENT,
            created__lte=(datetime.now() - timedelta(days=1))
        ).delete()

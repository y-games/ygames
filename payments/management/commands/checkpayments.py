from django.core.management import BaseCommand

from game.models import Tournament

from payments.consts import GATEWAY_PAYMENT
from payments.models import Payment
from payments.processor import (is_team_payment_paid,
                                mark_team_paid,
                                send_payment_received)


class Command(BaseCommand):
    help = 'Checks payments state'

    def handle(self, *args, **options):
        tourns = Tournament.objects.filter_enabled_registration()

        for payment in Payment.objects.filter(
                pay_date__isnull=True,
                team__game_rules__tournament__in=tourns,
                method=GATEWAY_PAYMENT):
            if is_team_payment_paid(payment):
                mark_team_paid(payment.team)
                send_payment_received(payment.team)

from django.utils.html import format_html
from django.utils.translation import ugettext as _
from django_tables2 import Column
from django_tables2 import tables


class PriceColumn(Column):
    
    def render(self, value):
        price_str = '{0:.2f}'.format(float(value) / 100)
        return format_html('<strong>{} &euro;</strong>', price_str)


class PaymentTable(tables.Table):
    name = Column(verbose_name=_('Item name'))
    price = PriceColumn(verbose_name=_('Price'))
    quantity = Column(verbose_name=_('Quantity'))

    class Meta:
        attrs = {'class': 'table table-hover'}

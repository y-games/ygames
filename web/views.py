from django import http
from django.conf import settings
from django.shortcuts import (render,
                              redirect)
from django.urls import translate_url
from django.utils.http import (is_safe_url,
                               urlunquote)
from django.utils.translation import (LANGUAGE_SESSION_KEY,
                                      check_for_language,
                                      activate)
from django.views.decorators.http import require_GET
from django.views.i18n import LANGUAGE_QUERY_PARAMETER

from web.forms import SearchForm
from .models import *


# TODO prehlad hracov
# TODO prehlad timov

# TODO Registracia na hru ( s potvrdenim )

# TODO prehlad zapasov v hre, ubehnute, buduce atd..

def index(req):
    if req.user.is_authenticated:
        return redirect('teams_overview')
    return redirect('register')


@require_GET
def show_news(request):
    # TODO create pagination after some amount of tags
    form_init = {}
    filter_ops = {}
    form = SearchForm(request.GET)
    if form.is_valid():
        tags = form.cleaned_data['tags']
        if tags:
            form_init['tags'] = ' '.join(('#{}'.format(t)
                                          for t in tags if t))
            filter_ops['tags__name__in'] = tags
        category = form.cleaned_data['category']
        if category:
            form_init['category'] = category
            filter_ops['category__pk'] = category.pk
    author = request.GET.get('author', None)
    if author:
        filter_ops['author__username'] = author
    if form_init:
        form = SearchForm(initial=form_init)
    else:
        form = SearchForm()
    if not filter_ops:
        news = News.objects.all().order_by('-date')
    else:
        news = News.objects.filter(**filter_ops)
        if 'tags__name__in' in filter_ops:
            news = news.annotate(score=models.Count('tags'))
            news = news.order_by('-score', '-date')
        else:
            news = news.order_by('-date')
    return render(request, 'web/news.html',
                  context={'news': news, 'form': form})


def game_detail(req, pk):
    games = Game.objects.all()
    game = Game.objects.get(pk=pk)
    news = News.objects.filter(category=game,
                               subpage='h').order_by('-date')[:10]

    # TODO show regnute timy a staty o regovani
    # TODO show pravidla a odkaz na ich stranku
    return render(req, 'game_detail.html', context={'games': games,
                                                    'info': game.info,
                                                    'news': news})


def select_language(request):
    next = request.POST.get('next', request.GET.get('next'))
    if ((next or not request.is_ajax()) and
            not is_safe_url(url=next, host=request.get_host())):
        next = request.META.get('HTTP_REFERER')
        if next:
            next = urlunquote(next)  # HTTP_REFERER may be encoded.
        if not is_safe_url(url=next, host=request.get_host()):
            next = '/'
    response = (http.HttpResponseRedirect(next) if next else
                http.HttpResponse(status=204))
    if request.method == 'POST':
        lang_code = request.POST.get(LANGUAGE_QUERY_PARAMETER)
        if lang_code and check_for_language(lang_code):
            if next:
                next_trans = translate_url(next, lang_code)
                if next_trans != next:
                    response = http.HttpResponseRedirect(next_trans)
            if hasattr(request, 'session'):
                request.session[LANGUAGE_SESSION_KEY] = lang_code
            else:
                response.set_cookie(
                    settings.LANGUAGE_COOKIE_NAME, lang_code,
                    max_age=settings.LANGUAGE_COOKIE_AGE,
                    path=settings.LANGUAGE_COOKIE_PATH,
                    domain=settings.LANGUAGE_COOKIE_DOMAIN,
                )
        activate(language=lang_code)
    return response

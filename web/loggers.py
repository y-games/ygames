import logging
from django.utils import timezone


class DatabaseLogHandler(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)

    def emit(self, record):
        from web.models import SystemErrorLog
        entry = SystemErrorLog(level=record.levelname,
                               message=record.message,
                               timestamp=timezone.now(),
                               pid=record.process,
                               tid=record.thread,
                               path=record.pathname,
                               stack_info=record.exc_text)
        entry.save()
        return

function AjaxSelect(jQuery, $element) {
    this._data = null;
    this.$ = jQuery;
    this._url = $element.attr('ajax-box-url');
    this._select_name = $element.attr('ajax-box-id');
    this._id = $element.attr('id');
    this._name = $element.attr('name');
    this._element = $element;
    this._changeElement = null;
}

AjaxSelect.prototype._render = function () {
    var _widget;
    if (this._data != undefined && this._data.length > 0) {
        _widget = $("<select class='form-control'></select>");
        this.$.each(this._data, function (index, option) {
            var $option = $("<option></option>").attr('value', option.value).text(option.text);
            _widget.append($option);
        })
    } else {
        _widget = $("<input class='form-control'>");
    }
    _widget.attr('name', this._name).attr('id', this._id)
        .attr('ajax-box-id', this._select_name).attr('ajax-box-url', this._url);
    return _widget;
};

AjaxSelect.prototype.load = function (code, callback) {
    var self = this;
    this.$.ajax({
        type: "POST",
        url: this._url,
        data: {code: code},
        success: function (data) {
            if (data == undefined || data.content == undefined) {
                self._data = null;
                callback();
                return;
            }
            self._data = data['content']['options'];
            callback();
        },
        error: function () {
            self._data = null;
            callback();
        }
    });
};

AjaxSelect.prototype._replace = function () {
    var $newElement = this._render();
    this._element.replaceWith($newElement);
    this._element = $newElement;
    this._rebind();
};

AjaxSelect.prototype._rebind = function () {
    var $element = this._changeElement,
        self = this;
    $element.off('change');
    $element.on('change', function (event) {
        self.load($element.val(), function () {
            self._replace();
        })
    });
};

AjaxSelect.prototype.bind = function ($changeElement) {
    if ($changeElement == undefined) {
        return;
    }
    if (this._changeElement != undefined) {
        return;
    }
    var self = this;
    this._changeElement = $changeElement;
    self.load($changeElement.val(), function () {
        self._replace();
    });
    $changeElement.on('change', function (event) {
        self.load($changeElement.val(), function () {
            self._replace();
        });
    });
};
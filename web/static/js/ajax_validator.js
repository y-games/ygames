(function ($, window) {
    $.formUtils.addValidator({
        name: 'ajax_validator',
        validatorFunction: function (value, $e, config, language, $form) {
            ajaxPost($e.attr('ajax-validator-url'), {name: $e.attr('name'), value: value}, function (data) {
                if (data['valid'] == undefined) {
                    return;
                }
                var name = $e.attr('name');
                var $div = $e.parent();
                var span_id = 'help_block_' + name;
                $('#' + span_id).remove();
                if (data.valid == true) {
                    $div.removeClass("has-error");
                    $div.addClass("has-success");
                    return;
                }
                $div.removeClass("has-success");
                $div.addClass("has-error");
                if (data['message'] != undefined) {
                    $e.attr('aria-describedby', span_id);

                    $div.append('<span id="' + span_id + '" class="help-block">' + data.message + '</span>')
                }
            });
        },
        errorMessage: '-',
        errorMessageKey: 'ajaxValid'
    });
})(jQuery, window);
(function ($) {
    var LANG = $('html').attr('lang');
    if (LANG in ['sk', 'hu']) {
        LANG = undefined;
    }
    $.validate({
        language: LANG,
        validateOnBlur: true,
        showHelpOnFocus: false,
        addSuggestions: false,
        modules: "security, date, sanitize"
    });
})(window.jQuery);
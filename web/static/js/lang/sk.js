(function($, window) {

  'use strict';

  $(window).bind('validatorsLoaded', function() {

    $.formUtils.LANG = {
      errorTitle: "Odoslanie formulára zlyhalo!",
      requiredField: 'Toto pole je povinné',
      requiredfields: "Nevyplnili ste všetky povinné polia",
      badTime: 'Neplatný čas',
      badEmail: 'Neplatná e-mailová adresa',
      badTelephone: 'Neplatné telefonní číslo',
      badSecurityAnswer: 'Chybná odpoveď na bezpečnostnú otázku',
      badDate: 'Nesprávný dátum',
      lengthBadStart: 'Zadaná hodnota musí byť v rosahu ',
      lengthBadEnd: ' znakov',
      lengthTooLongStart: 'Zadaná hodnota je vetšia než ',
      lengthTooShortStart: 'Zadaná hodnota je menšia než ',
      notConfirmed: 'Zadané hodnoty neboli potvrdené',
      badDomain: 'Neplatná doména',
      badUrl: 'Neplatná URL',
      badCustomVal: 'Zadaná hodnota je chybná',
      andSpaces: ' a medzery',
      badInt: 'Neplatné číslo',
      badSecurityNumber: 'Neplatné číslo zabezpečenia',
      badUKVatAnswer: 'Neplatné číslo DIČ ',
      badStrength: 'Vaše heslo nie je dostatočne silné',
      badNumberOfSelectedOptionsStart: 'Musíte vybrať nejméne ',
      badNumberOfSelectedOptionsEnd: ' odpoveď',
      badAlphaNumeric: 'Zadaná hodnota môže obsahovať len alfanumerické znaky ',
      badAlphaNumericExtra: ' a ',
      wrongFileSize: 'Súbor je príliš veľký (max %s)',
      wrongFileType: 'Len súbory typu %s',
      groupCheckedRangeStart: 'Prosím, vyberte ',
      groupCheckedTooFewStart: 'Vyberte prosím nejmenej ',
      groupCheckedTooManyStart: 'Vyberte prosím maximálne ',
      groupCheckedEnd: ' adresár(y)',
      badCreditCard: 'Číslo kreditnej karty je neplatné',
      badCVV: 'Číslo CVV je neplatné',
      wrongFileDim: 'Nesprávne rozmery obrázku,',
      imageTooTall: 'obraz nemôže byť vyšší než',
      imageTooWide: 'obraz nemôže byť širší než',
      imageTooSmall: 'obraz je príliž malý',
      min: 'min',
      max: 'max',
      imageRatioNotAccepted: 'Pomer obrázku je nesprávny',
      badBrazilTelephoneAnswer: 'Neplatné telefónne číslo',
      badBrazilCEPAnswer: 'Neplatné CEP',
      badBrazilCPFAnswer: 'Neplatné CPF'
    };
  });

})(jQuery, window);
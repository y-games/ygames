import re
from django import forms
from django.utils.translation import ugettext as _

from .models import Category


class SearchForm(forms.Form):
    _TAG_REGEX = re.compile(r'#(?P<tag>\w+)')

    tags = forms.CharField(max_length=254,
                           widget=forms.TextInput(attrs={
                               'class': 'form-control',
                               'placeholder': '#tag1 #tag2 #tag3'
                           }),
                           label=_('Tags'),
                           required=False)
    category = forms.ModelChoiceField(Category.objects.all(),
                                      widget=forms.Select(attrs={
                                          'class': 'form-control'
                                      }),
                                      label=_('Category'),
                                      required=False)

    def clean_tags(self):
        tags_input = self.cleaned_data['tags']
        tag_iter = self._TAG_REGEX.finditer(tags_input)
        tags = [tag_match.group('tag') for tag_match in tag_iter]
        print(tags)
        return tags

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from tinymce.models import HTMLField

from game.models import Game

# TODO autocomplete fields in admin

# Y-profil
from web.managers import (NewsManager,
                          TagManager)


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name_plural = _('Categories')
        verbose_name = _('Category')

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=100, unique=True)

    objects = TagManager()

    def __str__(self):
        return self.name


class News(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(User)
    text = HTMLField(blank=True)
    date = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, null=True)
    tags = models.ManyToManyField(Tag)

    objects = NewsManager()

    def __str__(self):
        return '[{}] {}'.format(self.date, self.title)

    class Meta:
        verbose_name_plural = 'news'


class SystemErrorLog(models.Model):
    level = models.CharField(max_length=25)
    message = models.TextField()
    timestamp = models.DateTimeField('timestamp', null=True, blank=True)
    pid = models.BigIntegerField()
    tid = models.BigIntegerField()
    path = models.TextField()
    stack_info = models.TextField(null=True, blank=True)




import re
from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _

from . import models
# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    ordering = ['name']

    list_display = ['name']


class TagAdmin(admin.ModelAdmin):
    ordering = ['name']
    list_display = ['name']


class NewsAdminForm(forms.ModelForm):
    _TAG_MARKUP = re.compile(r'#(?P<tag>\w+)')

    tags_field = forms.CharField(max_length=254)

    def __init__(self, *args, **kwargs):
        init = kwargs.setdefault('initial', {})
        if 'instance' in kwargs and hasattr(kwargs['instance'], 'tags'):
            inst = kwargs['instance']
            init.update({'tags_field': ' '.join(('#{}'.format(t.name)
                                                for t in inst.tags.all()))})
        super(NewsAdminForm, self).__init__(*args, **kwargs)

    class Meta:
        model = models.News
        exclude = ['date', 'author']

    def clean_tags_field(self):
        tags = self.cleaned_data['tags_field']
        cleaned = [tm.group('tag') for tm in self._TAG_MARKUP.finditer(tags)]
        return models.Tag.objects.get_or_create_by_name(cleaned)


class NewsAdmin(admin.ModelAdmin):
    ordering = ['-date']
    list_display = ('title', 'author', 'date', 'category', 'tags_line')
    list_filter = ['category']
    form = NewsAdminForm

    exclude = ['tags', 'author']

    def tags_line(self, obj):
        return ', '.join((tag.name for tag in obj.tags.all()))
    tags_line.short_description = _('Tags')

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        super().save_model(request, obj, form, change)
        obj.tags = form.cleaned_data['tags_field']
        obj.save()

admin.site.register(models.News, NewsAdmin)


class SystemErrorLogAdmin(admin.ModelAdmin):
    ordering = ('-timestamp', )
    list_display = ('level', 'message', 'timestamp')
    list_filter = ('level', )
    search_fields = ('level', 'message', 'path')

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        return self.readonly_fields + ('level', 'message', 'timestamp', 'pid',
                                       'tid', 'path', 'stack_info')

admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.Tag, TagAdmin)
admin.site.register(models.SystemErrorLog, SystemErrorLogAdmin)

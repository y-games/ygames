from django.db import models


class TagManager(models.Manager):

    def get_or_create_by_name(self, tag_names):
        tag_set = set(tag_names)
        tag_qs = self.filter(name__in=tag_set)
        existing_tags = []
        for e_tag in tag_qs:
            tag_set.remove(e_tag.name)
            existing_tags.append(e_tag)
        new_tags = [self.model(name=n) for n in tag_set]
        created = self.bulk_create(new_tags)
        return existing_tags + created


class NewsManager(models.Manager):

    def filter_by_tag_names(self, tags):
        return self.filter(tags__name__in=tags).annotate(
            num_tags=models.Count('tags')).filter(num_tags=len(tags))

    def filter_by_tags(self, tags):
        return self.filter(tags__in=tags).annotate(
            num_tags=models.Count('tags')).filter(num_tags=len(tags))

    def filter_by_category_name(self, category):
        return self.filter(category__name=category)

    def find_similar(self, news):
        category = news.category
        tags = news.tags.all()
        return self.filter(category=category, tags__in=tags).annotate(
            score=models.Count('tags')).order_by('-score', '-date')

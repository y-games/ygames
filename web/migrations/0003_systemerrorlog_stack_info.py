# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-07 09:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_systemerrorlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='systemerrorlog',
            name='stack_info',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]

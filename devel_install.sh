#!/usr/bin/env bash

BASEDIR=$(dirname $0)

PGDATABASE="ygames"
PGPASS="password"
PGHOST="localhost"
PGUSER="$USER"
PGPORT=5432

sudo apt install python3 python3-dev python3-pip libpq-dev build-essential python3-virtualenv virtualenv

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -d|--database)
    PGDATABASE="$2"
    shift # past argument
    ;;
    -h|--host)
    PGHOST="$2"
    shift # past argument
    ;;
    -u|--user)
    PGUSER="$2"
    shift # past argument
    ;;
    -W|--password)
    PGPASS="$2"
    ;;
    -p|--port)
    PGPORT="$2"
    shift
    ;;
    *)
    echo "UNKNOWN OPTION $key" >&2
    exit 1
    ;;
esac
shift # past argument or value
done

if [ ! -f $BASEDIR/env ]; then
    virtualenv -p python3 env
fi

mkdir -p $BASEDIR/media/

source $BASEDIR/env/bin/activate

mkdir -p ~/YGames/
if [ ! -f ~/YGames/settings.json ]; then
    SECRET_KEY=$(python -c 'import random; import string; s = set(string.digits + string.ascii_letters + string.punctuation);print("".join([random.SystemRandom().choice("".join(s - set("\""))) for _ in range(100)]))')
    cat > ~/YGames/settings.json << EOF
{
  "DATABASES": {
    "default": {
      "ENGINE": "django.db.backends.postgresql_psycopg2",
      "NAME": "$PGDATABASE",
      "USER": "$PGUSER",
      "PASSWORD": "$PGPASS",
      "HOST": "$PGHOST",
      "PORT": "$PGPORT"
    }
  },
  "SECRET_KEY": "$SECRET_KEY",
  "SITE_ID": 1,
  "ADMINS": [
    [
      "Add admin here",
      "email here"
    ]
  ],
  "MANAGERS": [
    [
      "Add manager here",
      "email here"
    ]
  ],
  "DOMAIN_NAME": "localhost"
}
EOF
fi

pip -r install $BASEDIR/requirements.txt

python ./manage.py migrate || :
python ./manage.py loaddata svk_provinces hun_provinces czk_provinces aus_provinces

deactivate

#!/usr/bin/env bash

WORKING_DIR=`pwd`

if [ ! -f ./ygames/wsgi.py ]; then
    echo "Script called from wrong directory"
    exit 1
fi

if [ -f ${WORKING_DIR}/templates/maintenance.html ]; then
    echo "Starting maintenance mode page."
    cp ${WORKING_DIR}/templates/maintenance.html ${WORKING_DIR}/maintenance.html
fi

echo "Stopping gunicorn workers."
sudo systemctl stop ygames ||:
sudo systemctl disable ygames

echo "Creating database backup."
$HOME/pgbackup.sh
if [ "$?" != "0" ]; then
    echo "Backup failed"
    exit 1
fi

if [ ! -f ${WORKING_DIR}/env ]; then
    echo "Creating new python virtual environment."
    virtualenv -p python3 env
fi

echo "Sourcing virtual environment."
source ./env/bin/activate
echo "Installing requirements."
pip install -r ${WORKING_DIR}/requirements.txt

echo "Performing update."
python ./manage.py migrate ||:
bower install
python ./manage.py collectstatic ||:
python ./manage.py loaddata svk_provicens czk_provinces hun_provinces aus_provinces
django-admin compilemessages

echo "Deactivating virtual environment."
deactivate

echo "Starting YGames portal."
sudo systemctl start ygames ||:
sudo systemctl enable ygames

echo "Removing maintenance mode page."
rm -f ./maintanance.html

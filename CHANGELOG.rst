CHANGELOG
=========

0.2.2
-----
    - Fixed few bugs in deployment scripts.
    - Add maintenance page.

0.2.1
-----
    - Created views for accommodations, templates are still missing.
    - Created :class:`AdditionalProduct` where addtional products of accommodation will be specified.
    - Created m2m table :class:`RegistrationProduct` wich connects registration with tournament accommodation
      and also stores quantity.
    - :file:`install.sh` creates now scripts for db backup and adds them to crontab
    - Created :file:`unistall.sh` for ygames web page removal.

0.2.0
-----
    - Created `accommodations` application.
    - Created ./install.sh for easy deployment.
    - Created empty `templates/maintenace.html` file, that will be served during maintenance.
    - Documentation changed to `Sphinx-Napoleon (Google style doc)`.
    - Removed all billing functions, this will be substitued by `django-billing` app, or something similar.

0.1.0
-----
    - Settings now will be loaded from config file instead of beeing hardcoded.
    - Reformating.

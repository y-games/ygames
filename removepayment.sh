#!/usr/bin/env sh
PROJECT_DIR=$(dirname "$0")

cd $PROJECT_DIR
$PROJECT_DIR/env/bin/python $PROJECT_DIR/manage.py removeold

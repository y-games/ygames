from django import template
from django.utils.html import escape
from django.utils.safestring import mark_safe

from ygames.settings import CLASSIFIER

register = template.Library()


@register.filter
def capitalize(string):
    return string.upper()


@register.filter('splitline')
def splitline(address):
    esc_address = escape(address)
    return mark_safe(esc_address.replace('\n', '<br>'))


@register.filter
def to_eur(price):
    return mark_safe('{0:.2f}'.format(float(price) / 100))


@register.filter
def classifier(tags, key=''):
    """Changes tags to css classes.

    Args:
        tags (str): messages tags separated with spaces

    Returns:
        str: Space separated css classes.
    """
    if key not in CLASSIFIER:
        raise ValueError('Unspecified key ("{}") in'
                         ' settings.CLASSIFIER'.format(key))
    css_class_map = CLASSIFIER[key]

    tags_lst = tags.split(' ')

    class_collector = [css_class_map.get(tag, '') for tag in tags_lst
                       if css_class_map.get(tag, None)]

    return ' '.join(class_collector)

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template

from ygames.settings import EMAIL_HOST_USER


class MailBuilder:

    def __init__(self, from_=EMAIL_HOST_USER, to=None, subject=None,
                 text_link=None, html_link=None):
        self._from = from_
        self._to = to or []
        self.subject = subject or ''
        self._text_template = text_link
        self._html_link = html_link
        self._attachments = []
        self._alternatives = []
        self._headers = {}

    @property
    def to(self):
        if type(self._to) is list:
            return self._to
        else:
            return [self._to]

    @property
    def auto_generated(self):
        return (self._headers.get('Auto-Submitted', default=None) ==
                'auto-generated')

    def put(self, header, value):
        headers_value = self._headers.get(header, default=None)
        if headers_value is None:
            self._headers[header] = value
        elif type(headers_value) is not list:
            self._headers[header] = [headers_value, value]
        else:
            self._headers[header].append(value)

    @auto_generated.setter
    def auto_generated(self, value):
        if value:
            self._headers['Auto-Submitted'] = 'auto-generated'
        elif 'Auto-Submitted' in self._headers:
            del self._headers['Auto-Submitted']

    @property
    def text_template(self):
        if not self._text_template:
            return None
        else:
            return get_template(self._text_template)

    def _render_text(self, context):
        txt_tpl = self.text_template
        if not txt_tpl:
            return ''

        return txt_tpl.render(context)

    @text_template.setter
    def text_template(self, value):
        self._text_template = value

    @property
    def html_template(self):
        if not self._html_link:
            return None
        else:
            return get_template(self._html_link)

    def _render_html(self, context):
        html_tpl = self.html_template
        if not html_tpl:
            return ''
        return html_tpl.render(context)

    @html_template.setter
    def html_template(self, value):
        self._html_link = value

    def remove(self, header):
        return self._headers.pop(header, default=None)

    def remove_value(self, header, value):
        if self._headers.get(header, default=value) == value:
            return self.remove(header)
        else:
            lst = self._headers[header]
            if value in lst:
                return lst.remove(value)

            return None

    def add_alternative(self, content, content_type):
        self._alternatives.append((content, content_type))

    def add_attachment(self, file_name, content, mimetype):
        self._attachments.append((file_name, content, mimetype))

    @property
    def headers(self):
        for header, values in self._headers.items():
            if type(values) is list:
                for value in values:
                    yield header, value
            else:
                yield header, values

    def create(self, context=None):
        ctx = Context(context or {})
        headers = {header: value for header, value in self.headers}
        body = self._render_text(ctx)
        html = self._render_html(ctx)
        alternatives = list(self._alternatives)
        if html:
            alternatives.append((html, 'text/html'))
        return EmailMultiAlternatives(self.subject, body, self._from, self.to,
                                      alternatives=alternatives,
                                      attachments=self._attachments,
                                      headers=headers)


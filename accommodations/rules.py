"""Basic rules for accommodation management."""
import rules


@rules.predicate
def can_delete_tournament_accommodation(user, accommodation):
    """Check whether is possible to delete tournament accommodation.

    Args:
        user (django.contrib.auth.models.User): Visitor account.
        accommodation (ygames.accommodations.models.TournamentAccommodation):
            Tournament accommodation.

    Returns:
        bool: True whether is possible to delete accommodation for current
            tournament.
    """
    return (rules.is_staff(user) and
            (not accommodation.tournament.is_registration_active() or
             not accommodation.accommodationreservation_set.exists()))


@rules.predicate
def is_team_admin(user, team):
    """Checks whether user is team admin.

    Args:
        user (django.contrib.auth.models.User): Visitor account.

    Returns:
        bool: True if user is a team admin, otherwise False.
    """
    return team.admins.filter(pk=user.pk).exists()


@rules.predicate
def is_reservation_owner(user, accommodation_reservation):
    """Checks whether is current visitor accommodation reservation owner.

    Args:
        user (auth.User): Visitor account.
        accommodation_reservation (ygames.accommodations.models.AccommodationReservation):
            Reservation of accommodation.

    Returns:
        bool: True if visitor is owner of reservation.
    """
    if not accommodation_reservation:
        return False
    return accommodation_reservation.user.id == user.id


@rules.predicate
def is_tournament_active(user, team):
    tournament = team.game_rules.tournament
    return tournament.is_registration_active()


@rules.predicate
def is_not_paid(user, accommodation_reservation):
    return accommodation_reservation.is_paid()


rules.add_perm('accommodations', rules.always_allow)
rules.add_perm('accommodations.change_accommodation', rules.is_staff)
rules.add_perm('accommodations.add_accommodation', rules.is_staff)
rules.add_perm('accommodations.delete_accommodation', rules.always_deny)

rules.add_perm('accommodations.add_tournamentaccommodation', rules.is_staff)
rules.add_perm('accommodations.change_tournamentaccommodation', rules.is_staff)
rules.add_perm('accommodations.delete_tournamentaccommodation',
               can_delete_tournament_accommodation)

rules.add_perm('accommodations.delete_accommodationreservation',
               is_reservation_owner)
rules.add_perm('accommodations.change_accommodationreservation',
               rules.is_staff | is_reservation_owner)
rules.add_perm('accommodations.add_accommodationreservation',
               rules.always_allow)

rules.add_perm('accommodations.add_additionalproduct', rules.is_staff)
rules.add_perm('accommodations.change_additionalproduct', rules.is_staff)
rules.add_perm('accommodations.delete_additionalproduct', rules.is_superuser)

rules.add_rule('can_see_reservation', is_reservation_owner)
rules.add_perm('accommodations.change_teamaccommodations',
               (rules.is_staff | is_team_admin) & is_tournament_active)
rules.add_perm('accommodations.add_teamaccommodations',
               (rules.is_staff | is_team_admin) & is_tournament_active)
rules.add_perm('accommodations.delete_teamaccommodations',
               (rules.is_staff | is_team_admin) & is_tournament_active)

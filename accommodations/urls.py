from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^reserve/(?P<pk>\d+)/$',
        views.reserve_accommodation, name='reserve_accommodation'),
    url(r'^$', views.show_accommodations, name='show_accommodations'),
    url(r'^(?P<pk>\d+)/$', views.show_accommodation,
        name='show_accommodation'),
    url(r'^delete/(?P<pk>\d+)/$', views.delete_reservation,
        name='delete_accommodation_reservation'),
    url(r'^reservation/(?P<pk>\d+)/add/(?P<prod_pk>\d+)/$', views.add_product,
        name='add_product'),
    url(r'^admin/team/(?P<pk>\d+)/$',
        views.TeamAccommodationReservationView.as_view(),
        name='team_accommodations'),
    url(r'^admin/team/(?P<pk>\d+)/additional/$',
        views.TeamAdditionalProducts.as_view(), name='team_products'),
    url(r'^admin/team/(?P<pk>\d+)/swap/$',
        views.SwapAccommodationView.as_view(),
        name='swap_owners')
]

from django import template

register = template.Library()


@register.filter
def get_item(dct, item):
    return dct.get(item)



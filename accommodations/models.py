"""Models for accommodations app."""
from autoslug import AutoSlugField
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from tinymce.models import HTMLField

from .managers import (AccommodationReservationManager,
                       ProductReservationManager)
from .varlidators import (validate_natural,
                          validate_non_negative)
from .consts import (ACCOM_PAID,
                     ACCOM_PAYMENT_CREATED,
                     ACCOM_RESERVED)

CONTENT_TYPES = (
    ('text/plain', _('Plain text')),
    ('text/html', _('Html text')),
    ('text/rst', _('reStructuredText'))
)


class Accommodation(models.Model):
    """Accommodation model object.

    This object contains basic information about certain accommodation, like
    name, contacts, description, etc.

    Attributes:
        name (str): Name of accommodation (i.e. name of Hotel).
        address (str): Formatted postal address information.
        description (str): Long or short description of accommodation, ideally
            in html.
        slug (str): Unique url slug.
        phone (str): Phone numbers separated by `\n` character.
        email (str): Email addresses separated by `\n` character.
        url (str): Url address if such accommodation has one.
        content_type (str, optional): Content type of following fields:
            :field:`address`, :field:`description`. Defaults to `"text/plain"`.
    """

    name = models.CharField(db_index=True, max_length=255)
    content_type = models.CharField(max_length=16, default='text/html',
                                    choices=CONTENT_TYPES)
    address = models.TextField(blank=True)
    description = HTMLField(blank=True)
    slug = AutoSlugField(populate_from='name', unique=True)
    phone = models.TextField(blank=True)
    email = models.TextField(blank=True)
    url = models.CharField(blank=True, max_length=255)

    def __str__(self):
        return self.name


def concat_slug(instance):
    return '{} {}'.format(instance.tournament.slug,
                          instance.accommodation.slug)


class TournamentAccommodation(models.Model):
    """Tournament accommodation model object.

    Serves as connection between accommodation and tournament. So there is no
    need to delete old accommodations or bind tournament directly to
    accommodation.

    Attributes:
        tournament (ygames.game.models.Tournament): Tournament object.
        accommodation (ygames.accommodations.models.Accommodation):
            Accommodation object.
        additional_info (str, optional): Additional information for
            accommodation, that are bound to current tournament. Defaults to
            `""`.
        price (int): Price of accommodation in cents (100 * currency).
        capacity (int): Maximal available capacity of accommodation.
        reservation_expiration_time (datetime.timedelta): Time window in which
            must be the reservation paid, otherwise it will be deleted.
        publish (bool, optional): If this reservation should be publicly
            visible. Defaults to `False`.
        slug (str): Url slug, most likely it will be automatically generated
            as concatenation of tournament slug and accommodation slug.
        content_type (str, optional): Content type of :field:`additional_info`
            field. Defaults to `"text/plain"`
    """

    tournament = models.ForeignKey('game.Tournament')
    accommodation = models.ForeignKey(Accommodation)
    additional_info = HTMLField(blank=True)
    content_type = models.CharField(max_length=16, default='text/html',
                                    choices=CONTENT_TYPES)
    check_in_date = models.DateField()
    check_out_date = models.DateField()
    price = models.IntegerField(validators=[validate_non_negative])
    ticket_id = models.IntegerField(null=False, db_index=True, default=-1)
    capacity = models.IntegerField(validators=[validate_natural])
    reservation_expiration_time = models.DurationField()
    publish = models.BooleanField(default=False)
    slug = AutoSlugField(populate_from=concat_slug,
                         unique=True)

    def __str__(self):
        return '[{}, {}] {}'.format(self.check_in_date, self.check_out_date,
                                    self.accommodation)

    def get_available_capacity(self):
        """Gets space for this accommodation.

        Returns:
            int: Space left.
        """
        return self.capacity - self.accommodationreservation_set.count()


class AdditionalProduct(models.Model):
    """Model for additional products, for certain tournament accommodation.

    Attributes:
        accommodation (TournamentAccommodation): Accommodation with which is
            product served.
        name (str): Name of product (short descriptive name).
        price (int): Price of additional product.
        description (str, optional): Description of poduct.
        content_type (str, optional): Content-Type of :field:`description`.
    """

    accommodation = models.ForeignKey(TournamentAccommodation)
    name = models.CharField(max_length=255)
    price = models.IntegerField(validators=[validate_non_negative])
    ticket_id = models.IntegerField(null=False, db_index=True, default=-1)
    description = HTMLField(blank=True)
    content_type = models.CharField(max_length=16, default='text/html',
                                    choices=CONTENT_TYPES)

    def __str__(self):
        return '[{}] {}'.format(self.accommodation, self.name)


class AccommodationReservation(models.Model):
    """Model object for accommodation reservation.

    Attributes:
        accommodation (TournamentAccommodation): Tournament accommodation.
        user (User): User that reserved this accommodation.
        additional (list of AdditionalProduct): Additional products for this
            reservation.
        reservation_date (datetime.datetime, optional): Date and time when this
            reservation was made. Defaults to current date and time.
        state (str, optional): Represents current state of reservation.
            Following states are available 'R' for reserved, 'P' for paid,
            'C' for checked in. Defaults to 'P'.
    """

    STATES = [(ACCOM_RESERVED, _('Reserved')),
              (ACCOM_PAYMENT_CREATED, _('Payment created')),
              (ACCOM_PAID, _('Paid'))]

    accommodation = models.ForeignKey(TournamentAccommodation)
    user = models.ForeignKey('auth.User', null=True)
    user_email = models.EmailField(blank=True)
    team = models.ForeignKey('teams.Team', null=True)
    reservation_date = models.DateTimeField(auto_now_add=True)

    objects = AccommodationReservationManager()

    class Meta:
        unique_together = ('accommodation', 'user')

    def __str__(self):
        if self.user:
            user_id = '{} {} ({})'.format(self.user.first_name,
                                          self.user.last_name,
                                          self.user.username)
        else:
            user_id = self.user_email
        return '[{}] {}'.format(user_id, self.accommodation, self.pk)

    def get_email(self):
        if self.user is not None:
            return self.user.email
        else:
            return self.user_email

    def remove_team(self):
        self.team = None
        self._handle_ownership()

    def remove_user(self):
        self.user = None
        self._handle_ownership()

    def _handle_ownership(self):
        if self.team or self.user:
            self.save()
        else:
            self.delete()

    def is_paid(self):
        return self.paymentitem_set.exists()


class ProductReservation(models.Model):
    product = models.ForeignKey(AdditionalProduct, on_delete=models.CASCADE)
    reservation = models.ForeignKey(AccommodationReservation,
                                    on_delete=models.CASCADE)
    quantity = models.IntegerField(validators=[validate_natural])

    objects = ProductReservationManager()

    class Meta:
        unique_together = ('product', 'reservation')

    def __str__(self):
        return '{} - {}'.format(str(self.reservation), self.product.name)

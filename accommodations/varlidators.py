"""Model validators module."""
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


def validate_natural(value):
    """Validates whether :arg:`value` is natural number."""
    if 0 >= value or int(value) != value:
        raise ValidationError(
            _('%(value)s is not a natural number.'),
            params={'value': value}
        )


def validate_non_negative(value):
    """Validates whether :arg:`value` is non negative integer."""
    if value < 0 or int(value) != value:
        raise ValidationError(
            _('%(value)s is not a non negative integer.'),
            params={'value': value}
        )

from django.apps import AppConfig


class AccomodationsConfig(AppConfig):
    name = 'accommodations'

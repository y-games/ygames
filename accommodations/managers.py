"""Accommodation model manager extensions."""
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Q

from .exceptions import (AlreadyReserved,
                         InvalidProductEntry,
                         AccommodationFull)


class AccommodationReservationManager(models.Manager):
    def has_reserved(self, user):
        """Return whether user already have reserved accommodation or not.

        Args:
            user (django.contrib.auth.models.User): User object.

        Returns:
            bool: True if user already have reservation.
        """
        return user.accommodationreservation_set.exists()

    def filter_by_email(self, email, **kwargs):
        return self.filter(Q(user__email=email) | Q(user_email=email),
                           **kwargs)

    def filter_by_emails(self, emails, **kwargs):
        return self.filter(Q(user_email__in=emails) |
                           Q(user__email__in=emails), **kwargs)

    def filter_by_user(self, user, **kwargs):
        return self.filter(Q(user=user) | Q(user_email=user.email), **kwargs)

    def create_reservation(self, user, accommodation, products=None):
        """Creates reservation for user.

        Checks whether user doesn't have already reservation and creates new
        one.

        Args:
            user (django.contrib.auth.models.User): User object.
            accommodation (ygames.accommodations.models.TournamentAccommodation):
                Accommodation for which should be reservation created.

        Returns:
            AccommodationReservation: Reservation object.

        Raises:
            AlreadyReserved: Exception is raised if user already have
            reservation.
        """
        if accommodation.get_available_capacity() <= 0:
            raise AccommodationFull

        return self.create(user=user, accommodation=accommodation)


class ProductReservationManager(models.Manager):

    def buy(self, product_id, quantity, reservation):
        product = reservation.additionalproduct_set.get(id=product_id)
        self.create(
            product=product,
            reservation=reservation,
            quantity=quantity
        )

    def add_product(self, reservation, product, quantity):
        try:
            prod_res = self.get(reservation=reservation, product=product)
            if not prod_res.quantity:
                prod_res.quantity = quantity
            else:
                prod_res.quantity += quantity
            prod_res.save()
            return prod_res
        except ObjectDoesNotExist:
            return self.create(product=product, reservation=reservation,
                               quantity=quantity)

    def buy_multiple(self, reservation, product_ids, quantities):
        products = reservation.additionalproduct_set.filter(id__in=product_ids)
        actual_ids = {p.id for p in products}
        leftovers = set(product_ids) - actual_ids
        if set(product_ids) - actual_ids:
            raise InvalidProductEntry(repr(list(leftovers)))
        selector = {p.id: p for p in products}
        product_models = (self.model(product=product, quantity=quantity,
                                     reservation=reservation)
                          for product, quantity in
                          zip(map(lambda x: selector[x]), quantities))
        self.bulk_create(product_models)

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.db import transaction
from django.db.models import Q
from django.forms import formset_factory
from django.http import Http404
from django.http import HttpResponseNotAllowed
from django.shortcuts import (render,
                              redirect,
                              get_object_or_404)
from django.utils.translation import ugettext as _
from django.views import View
from django.views.generic.detail import SingleObjectMixin
from rules.contrib.views import (permission_required,
                                 objectgetter, PermissionRequiredMixin)

from game.models import Tournament
from payments.models import PaymentItem
from teams.models import Team
from teams.consts import (TEAM_PAYMENT_CREATED,
                          TEAM_PAYMENT_PAID)
from .forms import (AddProductReservationForm,
                    member_model_form_factory,
                    TeamMemberAccommodationFormSet, team_products_form_factory,
                    TeamAdditionalProductFormSet,
                    team_reservations_form_factory,
                    SwapAccommodationOwnerFormSet)
from .models import (AccommodationReservation,
                     ProductReservation,
                     TournamentAccommodation,
                     AdditionalProduct)
from .consts import (ACCOM_RESERVED,
                     ACCOM_PAID,
                     ACCOM_PAYMENT_CREATED)


@login_required()
def reserve_accommodation(request, pk):
    return _create_reservation(request, pk)


def _create_reservation(request, pk):
    accommodation = get_object_or_404(TournamentAccommodation, pk=pk)
    if not Tournament.objects.exist_enabled_registration():
        return _render_all_accommodations(request,
                                          accommodation.tournament,
                                          error=_('Reservations are'
                                                  ' unavailable now.'))
    if not accommodation.publish:
        return _render_all_accommodations(request,
                                          accommodation.tournament,
                                          error=_('You can not reserve,'
                                                  ' not published'
                                                  ' accommodation.'))

    paid_reservations = AccommodationReservation.objects.filter_by_user(
        request.user, paymentitem__isnull=False)

    if paid_reservations.exists():
        return _render_all_accommodations(request, accommodation.tournament,
                                          error=_('You have already paid,'
                                                  ' accommodation!'))

    with transaction.atomic():
        if accommodation.get_available_capacity() <= 0:
            return _render_all_accommodations(request,
                                              accommodation.tournament,
                                              error=_(
                                                  "We are sorry, but this"
                                                  " accommodation reached"
                                                  " its limits."))
        try:
            team = Team.objects.get(
                members__player__user=request.user,
                game_rules__tournament=accommodation.tournament)
        except ObjectDoesNotExist:
            team = None
        AccommodationReservation.objects.create(
            user=request.user,
            user_email=request.user.email,
            team=team,
            accommodation=accommodation
        )

    return _render_all_accommodations(request, accommodation.tournament,
                                      success=_('Your accommodation was'
                                                ' reserved successfully'))


def show_accommodations(request):
    try:
        enabled_qs = Tournament.objects.filter_enabled_registration()
        tournament = enabled_qs.get()
        return _render_all_accommodations(request, tournament)
    except ObjectDoesNotExist:
        raise Http404


def _render_all_accommodations(request, tournament, error=None, success=None):
    if error:
        messages.error(request, error)

    if success:
        messages.success(request, success)

    if request.user.is_anonymous:
        reserved = {}
        can_reserve = False
    else:
        user_reservations = AccommodationReservation.objects.filter(
            user=request.user, accommodation__tournament=tournament)
        reserved = {r.accommodation.pk: r.pk for r in user_reservations}
        user_reserv_query = user_reservations.filter(
            paymentitem__payment__pay_date__isnull=False)
        can_reserve = (tournament.is_registration_active() and
                       not user_reserv_query.exists())
    return render(
        request,
        'accommodations/accommodations.html',
        context={
            'user': request.user,
            'can_reserve': can_reserve,
            'accommodations': tournament.tournamentaccommodation_set.filter(
                publish=True),
            'reserved': reserved,
            'error': error,
            'success': success,
        })


@permission_required('accommodations.delete_accommodationreservation',
                     fn=objectgetter(AccommodationReservation))
def delete_reservation(request, pk):
    reservation = get_object_or_404(AccommodationReservation, pk=int(pk))
    if not reservation.is_paid():
        return _render_all_accommodations(request,
                                          reservation.accommodation.tournament,
                                          error=_('You have already created'
                                                  ' payment for this'
                                                  ' accommodation'))
    payment_qs = PaymentItem.objects.filter_by_billable(reservation)
    if not payment_qs.exists():
        reservation.delete()
        return redirect('show_accommodations')
    else:
        latest = Tournament.objects.last()
        return _render_all_accommodations(
            request,
            latest,
            error=_('We are sorry, but at this point you are unable to'
                    ' delete your paid accommodation.'))


def show_accommodation(request, pk):
    accommodation = get_object_or_404(TournamentAccommodation, pk=pk)
    return _render_accommodation(request, accommodation, request.user)


def _render_accommodation(request, accommodation, user, forms=None):
    if request.user.is_anonymous:
        res = None
        res_prods = {}
        can_reserve = False
    else:
        user_reservations = AccommodationReservation.objects.filter(
            user=user, accommodation__tournament=accommodation.tournament)
        payment_created_reservations = user_reservations.filter(
            paymentitem__isnull=False
        )
        can_reserve = (accommodation.tournament.is_registration_active() and
                       not payment_created_reservations.exists())
        my_reservation = accommodation.accommodationreservation_set.filter(
            user=user
        )
        if my_reservation.exists():
            res = my_reservation.get()
            res_prods = {p.product.pk: p.quantity
                         for p in res.productreservation_set.all()}
        else:
            res = None
            res_prods = {}
    return render(request, 'accommodations/accommodation.html',
                  context={
                      'tournament_accommodation': accommodation,
                      'accommodation': accommodation.accommodation,
                      'products': accommodation.additionalproduct_set.all(),
                      'reservation': res,
                      'can_reserve': can_reserve,
                      'empty_form': AddProductReservationForm(),
                      'forms': forms or {},
                      'reserved_products': res_prods,
                      'user': user
                  })


@permission_required('accommodations.change_accommodationreservation',
                     fn=objectgetter(AccommodationReservation))
def add_product(request, pk, prod_pk):
    reservation = get_object_or_404(AccommodationReservation, pk=pk)
    if not reservation.accommodation.tournament.is_registration_active():
        return _render_all_accommodations(request, Tournament.objects.last(),
                                          error=_('This tournament already'
                                                  ' ended'))
    payment_qs = PaymentItem.objects.filter_by_billable(reservation)
    if payment_qs.exists():
        return _render_all_accommodations(request, Tournament.objects.last(),
                                          error=_('You can not change product'
                                                  ' reservation in this state')
                                          )
    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])

    product = get_object_or_404(AdditionalProduct, pk=prod_pk)

    form = AddProductReservationForm(request.POST)
    if form.is_valid():
        quantity = form.cleaned_data['quantity']
        ProductReservation.objects.add_product(reservation, product, quantity)
        return _render_accommodation(request, reservation.accommodation,
                                     request.user)
    else:
        return _render_accommodation(request, reservation.accommodation,
                                     request.user, {prod_pk: form})


@permission_required('accommodations.change_accommodationreservation',
                     fn=objectgetter(AccommodationReservation))
def delete_product_reservation(request, pk, product_pk):
    reservation = get_object_or_404(AccommodationReservation, pk=pk)
    payment_exists = PaymentItem.objects.is_created(reservation)
    if payment_exists:
        return _render_all_accommodations(request, Tournament.objects.last(),
                                          error=_('You cann\'t product'
                                                  ' reservations in this'
                                                  ' state'))
    ProductReservation.objects.filter(pk=product_pk).delete()
    return _render_accommodation(request, reservation.accommodation,
                                 request.user)


class TeamAccommodationReservationView(SingleObjectMixin,
                                       PermissionRequiredMixin, View):
    model = Team
    permission_required = 'accommodations.change_teamaccommodations'

    def get(self, request, pk):
        team = self.get_object()
        tournament = team.game_rules.tournament
        emails = team.get_emails()
        member_accs = AccommodationReservation.objects.filter_by_emails(emails)
        team_accs = member_accs.filter(
            Q(team__isnull=True) | Q(team=team),
            paymentitem__isnull=True
        )

        team_acc_data = [{'email': acc_res.get_email(),
                          'accommodation': acc_res.accommodation}
                         for acc_res in team_accs]

        AccommodationReservationFormSet = formset_factory(
            member_model_form_factory(
                team,
                tournament.tournamentaccommodation_set.filter(publish=True)),
            TeamMemberAccommodationFormSet)
        return render(
            request,
            'accommodations/team_accommodation.html',
            context={
                'team_formset': AccommodationReservationFormSet(
                    initial=team_acc_data),
                'team': team
            }
        )

    def post(self, request, pk):
        team = self.get_object()
        tournament = team.game_rules.tournament
        AccommodationReservationFormSet = formset_factory(
            member_model_form_factory(
                team,
                tournament.tournamentaccommodation_set.filter(publish=True)),
            TeamMemberAccommodationFormSet,
        )
        if team.paymentitem_set.exists():
            messages.error(request, _('You can not add new accommodations.'))
            return redirect('teams_overview')
        formset = AccommodationReservationFormSet(request.POST)

        if formset.is_valid():
            new_reservations = []
            for form in formset:
                email = form.cleaned_data.get('email')
                accommodation = form.cleaned_data.get('accommodation')

                if email and accommodation:
                    acc = AccommodationReservation(
                        user_email=email,
                        team=team,
                        user=_get_user_by_email(email),
                        accommodation=accommodation
                    )
                    new_reservations.append(acc)
            emails = team.get_emails()
            try:
                with transaction.atomic():
                    AccommodationReservation.objects.filter(
                        Q(user__email__in=emails) | Q(user_email__in=emails)
                    ).delete()
                    AccommodationReservation.objects.bulk_create(
                        new_reservations)
                    messages.success(request, _('Team accommodations'
                                                ' successfully updated'))
            except IntegrityError:
                messages.error(request, _('There was error during team'
                                          ' accommodations update'))
                return redirect('teams_overview')

        return render(request, 'accommodations/team_accommodation.html',
                      context={'team_formset': formset, 'team': team})


def _get_user_by_email(email):
    try:
        return User.objects.get(email=email)
    except ObjectDoesNotExist:
        return None


class TeamAdditionalProducts(SingleObjectMixin, PermissionRequiredMixin, View):
    model = Team
    permission_required = "accommodations.change_teamaccommodations"

    def get(self, request, pk):
        team = self.get_object()
        emails = team.get_emails()
        member_res = AccommodationReservation.objects.filter_by_emails(emails)
        team_reservations = member_res.filter(
            Q(team__isnull=True) | Q(team=team),
            paymentitem__isnull=True
        )

        TeamProductReservationFormSet = formset_factory(
            team_products_form_factory(team_reservations),
            TeamAdditionalProductFormSet)

        p_reses = ProductReservation.objects.filter(
            reservation__in=team_reservations
        )

        reservation_data = [
            {
                'reservation': p_res.reservation,
                'quantity': p_res.quantity,
                'product': p_res.product
            }
            for p_res in p_reses
        ]

        return render(
            request, 'accommodations/team_products.html',
            context={'team_formset': TeamProductReservationFormSet(
                initial=reservation_data
            ),
                'team': team})

    def post(self, request, pk):
        team = self.get_object()

        if team.paymentitem_set.exists():
            messages.error(request,
                           _('Payment for this team was already created/paid.')
                           )
            return redirect('teams_overview')

        emails = team.get_emails()
        team_reservations = AccommodationReservation.objects.filter(
            (Q(user__email__in=emails) | Q(user_email__in=emails)) &
            Q(paymentitem__isnull=True)
        )
        TeamProductFormSet = formset_factory(
            team_products_form_factory(team_reservations),
            TeamAdditionalProductFormSet
        )

        formset = TeamProductFormSet(request.POST)
        if formset.is_valid():
            new_reservations = []
            for form in formset.forms:
                reservation = form.cleaned_data.get('reservation', None)
                quantity = form.cleaned_data.get('quantity', 0)
                product = form.cleaned_data.get('product', None)

                if all((reservation, quantity, product)):
                    res = ProductReservation(
                        reservation=reservation,
                        quantity=quantity,
                        product=product
                    )
                    new_reservations.append(res)
            try:
                with transaction.atomic():
                    ProductReservation.objects.filter(
                        reservation__in=team_reservations
                    ).delete()
                    ProductReservation.objects.bulk_create(new_reservations)
                    messages.success(request, _('Product reservations saved'
                                                ' successfully'))
            except IntegrityError:
                messages.error(request, _('There was error during team'
                                          ' reservation update.'))
                return redirect('team_overview')

        return render(request, 'accommodations/team_accommodation.html',
                      context={'team_formset': formset, 'team': team})


class SwapAccommodationView(SingleObjectMixin, PermissionRequiredMixin, View):
    model = Team
    permission_required = 'accommodations.change_teamaccommodations'

    def get(self, request, pk):
        team = self.get_object()
        TeamReservationsFormSet = formset_factory(
            team_reservations_form_factory(team),
            SwapAccommodationOwnerFormSet,
            extra=0
        )

        reservations = [{'email': res.get_email(), 'accommodation': res}
                        for res in team.accommodationreservation_set.filter(
                paymentitem__isnull=False)]
        formset = TeamReservationsFormSet(initial=reservations)

        return render(request, 'accommodations/team_swapowners.html',
                      context={'formset': formset, 'team': team})

    def post(self, request, pk):
        team = self.get_object()
        TeamReservationFormSet = formset_factory(
            team_reservations_form_factory(team),
            SwapAccommodationOwnerFormSet
        )

        formset = TeamReservationFormSet(request.POST)

        if not formset.is_valid():
            messages.error(request, _('Some error occured during validation.'))
            return render(request, 'accommodations/team_swapowners.html',
                          context={'formset': formset, 'team': team})

        for form in formset.forms:
            accommodation = form.cleaned_data['accommodation']
            email = form.cleaned_data['email']
            user = _get_user_by_email(email)
            accommodation.user = user
            accommodation.user_email = email
            accommodation.save()

        messages.success(request,
                         _('Accommodation owners changed successfully.'))

        return render(request, 'accommodations/team_swapowners.html',
                      context={'formset': formset, 'team': team})

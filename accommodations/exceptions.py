"""Module containing all accommodation app exceptions."""


class AlreadyReserved(Exception):
    """Exception thrown mostly when user already have reservation."""
    pass


class AccommodationFull(Exception):
    """Exception thrown when accommodation reached it's capacity."""
    pass


class InvalidProductEntry(Exception):
    pass

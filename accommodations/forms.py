"""Accommodations forms module."""
from crispy_forms.helper import FormHelper
from django import forms
from django.forms.widgets import ChoiceInput
from django.utils.translation import ugettext as _

from accommodations.models import AdditionalProduct


class AddProductsForm(forms.Form):

    def __init__(self, tournament_accommodation, *args, **kwargs):
        super().__init__(*args, **kwargs)
        choices = (
            (product.id, '{} - {}'.format(product.name, product.price))
            for product in tournament_accommodation.additionalproduct_set)
        for product_id, label in choices:
            self.fields['%d' % product_id] = forms.IntegerField(label=label)


class AddProductReservationForm(forms.Form):

    quantity = forms.IntegerField(min_value=1, max_value=200, initial=1)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.label_class = 'col-lg-2 col-sm-4'
        self.helper.field_class = 'col-lg-10 col-sm-8'
        self.helper.form_tag = False
        self.helper.html5_required = True
        self.helper.help_text_inline = True
        self.helper.error_text_inline = True


def member_model_form_factory(team, accommodations, label=None):
    """

    Args:
        accommodations (queryset):
        team (ygames.teams.models.Team): team

    Returns:
        forms.Form:
    """
    if not label:
        label = _('Accommodation')

    class TeamMemberAccommodationForm(forms.Form):

        email = forms.ChoiceField(
            choices=team.get_emails_with_names(),
            label=_('Team member email'),
            widget=forms.Select(attrs={'class': 'form-control'})
        )
        accommodation = forms.ModelChoiceField(
            accommodations,
            label=label,
            widget=forms.Select(attrs={'class': 'form-control'})
        )

    return TeamMemberAccommodationForm


def team_products_form_factory(reservations):
    """

    Args:
        reservations (list of ygames.accommodations.models.AccommodationReservation):
            reservation instance.

    Returns:

    """
    acc_pks = {r.accommodation.pk for r in reservations}

    class ProductReservationForm(forms.Form):

        reservation = forms.ModelChoiceField(
            reservations,
            widget=forms.Select(attrs={'class': 'form-control'})
        )
        quantity = forms.IntegerField(
            min_value=0,
            widget=forms.NumberInput(attrs={'class': 'form-control'})
        )
        product = forms.ModelChoiceField(
            AdditionalProduct.objects.filter(accommodation__pk__in=acc_pks),
            widget=forms.Select(attrs={'class': 'form-control'})
        )

    return ProductReservationForm


class TeamAdditionalProductFormSet(forms.BaseFormSet):

    def clean(self):
        if any(self.errors):
            return

        products = set()

        for form in self.forms:
            if form.cleaned_data:
                product = form.cleaned_data['product']
                reservation = form.cleaned_data['reservation']
                pr = (product, reservation)

                if pr in products:
                    raise forms.ValidationError(_('Product should be unique'))

                products.add(pr)

                if product.accommodation != reservation.accommodation:
                    raise forms.ValidationError(_("Product and reservation"
                                                  " doesn't reference to the"
                                                  " same accommodation"))


class TeamMemberAccommodationFormSet(forms.BaseFormSet):

    def clean(self):

        if any(self.errors):
            return

        email_set = set()

        for form in self.forms:
            if form.cleaned_data:
                email = form.cleaned_data['email']
                accommodation = form.cleaned_data['accommodation']

                if (email, accommodation) in email_set:
                    raise forms.ValidationError(_('Emails should be unique'
                                                  ' for every accommodation'))
                email_set.add((email, accommodation))

                if bool(email) ^ bool(accommodation):
                    raise forms.ValidationError(_('Email and accommodation'
                                                  ' should be both set or both'
                                                  ' left blank'))


def team_reservations_form_factory(team):
    """Creates form for swapping team members in team accommodations.

    Args:
        team (ygames.teams.models.Team): Team instance.

    Returns:
        forms.Form: Form for swapping team members.
    """
    reservations = team.accommodationreservation_set.all()
    return member_model_form_factory(team, reservations,
                                     label=_('Team accommodation reservation'))


class SwapAccommodationOwnerFormSet(forms.BaseFormSet):

    def clean(self):

        if any(self.errors):
            return

        reservations = set()

        for form in self.forms:
            if form.cleaned_data:
                accommodation = form.cleaned_data['accommodation']

                if accommodation in reservations:
                    raise forms.ValidationError(_('Reservation must be'
                                                  ' unique.'))
                reservations.add(accommodation)
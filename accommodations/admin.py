from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from rules.contrib.admin import ObjectPermissionsModelAdmin

from payments.models import PaymentItem
from teams.models import Team
from .models import (Accommodation,
                     AdditionalProduct,
                     AccommodationReservation,
                     TournamentAccommodation,
                     ProductReservation)


class AccommodationAdmin(ObjectPermissionsModelAdmin):
    list_display = ('name', 'slug', 'url')
    search_fields = ['name']
    fieldsets = (
        (None, {
            'fields': ('name', 'address', 'description', 'phone', 'email',
                       'url')
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': ('content_type',)
        }),
    )


class AdditionalProductInline(admin.StackedInline):
    model = AdditionalProduct
    fk_name = 'accommodation'
    extra = 0

    fieldsets = (
        (None, {
            'fields': ('name', 'description', 'price')
        }),
        (_("Advanced options"), {
            'fields': ('accommodation', 'ticket_id', 'content_type'),
            'classes': ('collapse',)
        })
    )

    def accommodation_name(self, obj):
        return obj.accommodation.accommodation.name

    def product_price(self, obj):
        return '{0:.2f} \u20ac'.format(float(obj.price) / 100)


class TournamentAccommodationAdmin(ObjectPermissionsModelAdmin):
    list_display = ('name', 'tournament', 'slug', 'price_per_night',
                    'capacity', 'available')
    list_filter = ('tournament', 'accommodation')
    fieldsets = (
        (None, {
            'fields': ('price', 'additional_info', 'capacity', 'publish',
                       'check_in_date', 'check_out_date')
        }),
        (_('Advanced options'), {
            'fields': ('tournament', 'accommodation', 'content_type',
                       'reservation_expiration_time', 'ticket_id'),
            'classes': ('collapse',)
        })
    )

    inlines = [
        AdditionalProductInline
    ]

    def available(self, obj):
        return obj.get_available_capacity()
    available.short_description = 'Available capacity'

    def name(self, obj):
        return obj.accommodation.name

    def price_per_night(self, obj):
        return '{0:.2f} \u20ac'.format(float(obj.price) / 100)


class ProductReservationInline(admin.StackedInline):
    model = ProductReservation
    fk_name = 'reservation'
    extra = 0


class PaidAccommodationFilter(SimpleListFilter):
    title = _('paid')
    parameter_name = 'paid'

    _PAID = 'p'
    _UNPAID = 'r'
    _CREATED = 'c'

    def lookups(self, request, model_admin):
        return [(PaidAccommodationFilter._PAID, _('Paid')),
                (PaidAccommodationFilter._UNPAID, _('Reserved')),
                (PaidAccommodationFilter._CREATED, _('Payment created'))]

    def queryset(self, request, queryset):
        if self.value() == 'p':
            return queryset.filter(
                paymentitem__payment__pay_date__isnull=False)
        if self.value() == 'r':
            return queryset.filter(paymentitem__isnull=True)
        if self.value() == 'c':
            return queryset.filter(paymentitem__isnull=False,
                                   paymentitem__payment__pay_date__isnull=True)
        return queryset


class AccommodationReservationAdmin(ObjectPermissionsModelAdmin):
    list_display = ('accommodation', 'user', 'user_email', 'full_name',
                    'is_paid', 'team_link')

    list_filter = ('accommodation__tournament', PaidAccommodationFilter,
                   'accommodation__accommodation')
    search_fields = ['team__name', 'team__acronym', 'user__username',
                     'user__first_name', 'user__last_name', 'user__email',
                     'user_email']

    inlines = [
        ProductReservationInline,
    ]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'user':
            kwargs['queryset'] = User.objects.order_by('username')
        if db_field.name == 'team':
            kwargs['queryset'] = Team.objects.filter_current().order_by(
                'name', 'game_rules')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def full_name(self, obj):
        if obj.user is None:
            return '-'
        else:
            return '{} {}'.format(obj.user.first_name, obj.user.last_name)

    def is_paid(self, obj):
        return PaymentItem.objects.is_paid(obj)

    is_paid.boolean = True

    def team_link(self, obj):
        if obj.team is None:
            return '-'
        return mark_safe('<a href="{}">{}</a>'.format(
            reverse('admin:teams_team_change', args=(obj.team.pk,)),
            obj.team
        ))
    team_link.short_description = _('Team')

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if obj.user is not None:
            obj.user_email = obj.user.email
            obj.save()


admin.site.register(Accommodation, AccommodationAdmin)
admin.site.register(TournamentAccommodation, TournamentAccommodationAdmin)
admin.site.register(AccommodationReservation, AccommodationReservationAdmin)

$(document).ready(function () {
    var team_url = '/teams/';
    var ajax_url = '/teams/overview/last/_data/';
    var fancy_no = '<span class="glyphicon glyphicon-remove" aria-hidden="True"></span>';
    var fancy_yes = '<span class="glyphicon glyphicon-ok" aria-hidden="True"></span>';

    var yn_factory = function (name) {
        return function (nTd, sData, oData, iRow, iCol) {
            if (oData[name]) {
                $(nTd).html(fancy_yes);
            } else {
                $(nTd).html(fancy_no);
            }
        }
    };

    var link_factory = function (url, name) {
        return function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html('<a href="' + oData[url] + '">' + oData[name] + '</a>')
        }
    };

    var teamTable = $('#teams-table').DataTable({
        lengthMenu: [[-1, 10, 25, 50], ["All", 10, 25, 50]],
        columns: [
            {
                data: "tournament",
                fnCreatedCell: link_factory("tournament_url", "tournament_name")
            },
            {
                data: "team",
                fnCreatedCell: link_factory("team_url", "team_name")
            },
            {data: "admin_emails"},
            {data: "admin_phones"},
            {
                data: "game_name",
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).text(oData.game_name + ' (' + oData.game_rule_name + ')');
                }
            },
            {
                data: "vip",
                fnCreatedCell: yn_factory("vip")
            },
            {data: "team_vs"},
            {data: "state"},
            {
                data: "paid",
                fnCreatedCell: yn_factory("paid")
            },
            {data: "created"}
        ],
        ajax: {
            url: ajax_url,
            dataSrc: ''
        }
    });

});
# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-14 22:22
from __future__ import unicode_literals

from django.db import migrations
from django.db.models import Q
from datetime import datetime


def make_paid(apps, schema_editor):
    Team = apps.get_model('teams', 'Team')
    Tourn = apps.get_model('game', 'Tournament')
    AccRes = apps.get_model('accommodations', 'AccommodationReservation')
    tournament = Tourn.objects.last()
    if not is_reg_active(tournament):
        return
    teams = Team.objects.filter(game_rules__tournament=tournament,
                                pay_day__isnull=False)
    for team in teams:
        team.state = 'p'
        team.save()
        emails = get_emails(team)
        acc_res = AccRes.objects.filter(Q(user_email__in=emails) |
                                        Q(user__email__in=emails))
        acc_res.update(team=team, state='P')


def is_reg_active(tourn):
    reg_start = datetime(tourn.registration_start.year,
                         tourn.registration_start.month,
                         tourn.registration_start.day)
    reg_end = datetime(tourn.registration_end.year,
                       tourn.registration_end.month,
                       tourn.registration_end.day,
                       hour=23,
                       minute=59,
                       second=59)
    return reg_start <= datetime.now() <= reg_end


def get_emails(team):
    emails = {m.player.user.email for m in team.members.all()}
    emails.update({i.email or i.user.email for i in team.invites.all()})
    emails.update({a.email for a in team.admins.all()})
    return list(emails)


class Migration(migrations.Migration):
    dependencies = [
        ('teams', '0013_team_state'),
    ]

    operations = [
        migrations.RunPython(make_paid)
    ]

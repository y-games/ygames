from django.utils.html import format_html
from django.utils.translation import ugettext as _
from django_tables2 import (Table,
                            Column,
                            BooleanColumn)


class PriceColumn(Column):

    def render(self, value):
        price_str = '{0:.2f}'.format(float(value) / 100)
        return format_html('<strong>{} &euro;</strong>', price_str)


class TeamsTable(Table):
    name = Column(verbose_name=_('Team name'))
    emails = Column(verbose_name=_('Captain emails'))
    game = Column(verbose_name=_('Game'))
    vip = BooleanColumn(verbose_name=_('VIP'))
    payer_reference = Column(verbose_name=_('Variable symbol'),
                             orderable=False)
    has_paid = BooleanColumn(verbose_name=_('Has paid'))
    price = PriceColumn(verbose_name=_('Price'))
    pay_date = Column(verbose_name=_('Payment processed date'))
    team_created = Column(verbose_name=_('Team created date'))

    class Meta:
        attrs = {'class': 'table table-hover'}


class PaymentTable(Table):
    team_name = Column(verbose_name=_('Team name'))
    payer = Column(verbose_name=_('Payer name'))
    game = Column(verbose_name=_('Game'))
    player_price = PriceColumn(verbose_name=_('Player price'))
    accom_price = PriceColumn(verbose_name=_('Accommodation price'))
    total_price = PriceColumn(verbose_name=_('Total price'))
    has_paid = BooleanColumn(verbose_name=_('Has paid'))
    pay_date = Column(verbose_name=_('Payment processed date'))
    team_created = Column(verbose_name=_('Team created date'))
    payer_reference = Column(verbose_name=_('Payer reference'),
                             orderable=False)
    emails = Column(verbose_name=_('Captain emails'))
    phones = Column(verbose_name=_('Captain phones'))

    class Meta:
        attrs = {'class': 'table table-hover'}

from ygames.widgets import tag


def get_ok():
    return '<span class="glyphicon glyphicon-ok" aria-hidden="True"></span>'


def get_no():
    return ('<span class="glyphicon glyphicon-remove" aria-hidden="True">'
            '</span>')


from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(name='yn_filter')
def yn_filter(boolean):
    if boolean:
        return mark_safe('<span class="glyphicon glyphicon-ok"'
                         ' aria-hidden="True"></span>')
    else:
        return mark_safe('<span class="glyphicon glyphicon-remove"'
                         ' aria-hidden="True"></span>')

from django import template

register = template.Library()


@register.filter(name='game_rule_name')
def game_name(game_rule):
    if not game_rule.vip:
        return '{} - {}'.format(game_rule.game.name, game_rule.name)
    else:
        return '[VIP] {} - {}'.format(game_rule.game.name, game_rule.name)

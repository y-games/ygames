from crispy_forms.helper import FormHelper
from django import forms
from django.urls import reverse
from django.utils.translation import ugettext as _

from .models import Team


class EditTeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ['name', 'acronym']

    invited_player_count = forms.CharField(widget=forms.HiddenInput())

    name = forms.CharField(required=True)

    acronym = forms.CharField(required=True)

    def __init__(self, game_rules, *args, **kwargs):
        """
        Constructor
        :param game_rules: game_rules model object
           :type game_rules: game.models.GameRules
        :param args:
        :param kwargs:
        """
        super(EditTeamForm, self).__init__(*args, **kwargs)

        self._game_rules = game_rules

        extra_fields = game_rules.player_count
        self.fields['invited_player_count'].initial = extra_fields

        for i in range(1, int(extra_fields + 1)):
            self.fields['player_{i}'.format(i=i)] = forms.EmailField(
                max_length=100, required=True,
                widget=forms.widgets.EmailInput(attrs={
                    'data-validation': 'email',
                }),
                label=_('Player %(player_id)d email' % {
                    'player_id': i}))

        self.fields['acronym'] = forms.CharField(
            max_length=100,
            required=True,
            widget=forms.widgets.TextInput(
                attrs={
                    'data-validation': 'required ajax_validator',
                    'data-sanitize': 'trim',
                    'ajax-validator-url': reverse('teams_validator', kwargs={
                        'pk': game_rules.pk})
                }),
            label=_('Team abbreviation'))

        self.fields['name'] = forms.CharField(
            max_length=100, required=True,
            widget=forms.widgets.TextInput(attrs={
                'data-validation': 'required ajax_validator',
                'data-sanitize': 'trim',
                'ajax-validator-url': reverse(
                    'teams_validator',
                    kwargs={
                        'pk': game_rules.pk
                    })
            }),
            label=_('Team name'))

        self.helper = FormHelper()
        self.helper.field_class = 'form-horizontal'
        self.helper.form_tag = False
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.html5_required = True
        self.helper.help_text_inline = True
        self.helper.error_text_inline = True

    def clean(self):
        cleaned_data = super(EditTeamForm, self).clean()

        if 'name' not in cleaned_data:
            raise forms.ValidationError(
                {'name': [_("Name field can not be blank")]})
        else:
            if Team.objects.filter(game_rules=self._game_rules,
                                   name=cleaned_data['name']).exists():
                raise forms.ValidationError({'name': [
                    _('This name is already in use by another team'), ]})

        if 'acronym' not in cleaned_data:
            raise forms.ValidationError(
                {'acronym': [_("Acronym field can not be blank")]})
        else:
            if Team.objects.filter(game_rules=self._game_rules,
                                   acronym=cleaned_data['acronym']).exists():
                raise forms.ValidationError({'acronym': [
                    _('This acronym is already in use by another team'), ]})

        return cleaned_data

from daterange_filter.filter import DateRangeFilter
from django.contrib import admin
from django.contrib import messages
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import (ugettext_lazy as _,
                                      ungettext as _pl)

from payments import processor

from teams import models


class TeamMemberInline(admin.StackedInline):
    model = models.TeamMember
    fk_name = 'team'
    extra = 0
    raw_id_fields = ['player']


class TeamInviteInline(admin.StackedInline):
    model = models.TeamInvite
    fk_name = 'team'
    extra = 0


class TeamPaidFilter(admin.SimpleListFilter):
    title = _('paid')
    parameter_name = 'paid'

    def lookups(self, request, model_admin):
        return [
            ('r', _('Without payment')),
            ('c', _('Created payment')),
            ('p', _('Paid'))
        ]

    def queryset(self, request, queryset):
        if self.value() == 'r':
            return queryset.filter(paymentitem__isnull=True)

        if self.value() == 'c':
            return queryset.filter(paymentitem__isnull=False,
                                   paymentitem__payment__pay_date__isnull=True)

        if self.value() == 'p':
            return queryset.filter(
                paymentitem__payment__pay_date__isnull=False)

        return queryset


class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'acronym', 'get_admins_emails',
                    'get_admins_phones', 'game_rules', 'is_paid', 'is_vip',
                    'get_vs', 'created')
    list_filter = (TeamPaidFilter, ('created', DateRangeFilter),
                   'game_rules__tournament', 'game_rules__game',
                   'game_rules__vip', 'game_rules',)
    inlines = [
        TeamMemberInline,
        TeamInviteInline,
    ]
    actions = ['make_paid', 'make_unpaid', 'create_payment', 'remove_payment',
               'send_payment_email']
    filter_horizontal = ('admins',)
    list_max_show_all = 500
    search_fields = ['name', 'id', 'acronym', 'game_rules__game__name',
                     'admins__email', 'admins__profile__phone_number']

    def is_vip(self, obj):
        return obj.game_rules.vip

    is_vip.boolean = True

    def is_paid(self, obj):
        return obj.paymentitem_set.filter(
            payment__pay_date__isnull=False).exists()

    is_paid.boolean = True

    def send_payment_email(self, request, queryset):
        rows_updated = 0
        for team in queryset.filter():
            processor.send_payment_mail(team)
            rows_updated += 1
        msg = _pl('Sent {count} email',
                  'Sent {count} emails',
                  rows_updated).format(count=rows_updated)
        self.message_user(request, msg)

    send_payment_email.short_description = _('Send payment emails to admins')

    def create_payment(self, request, queryset):
        rows_updated = 0
        for team in queryset.all():
            try:
                admin = team.admins.first()
                processor.create_team_payment(team, admin)
                processor.send_payment_mail(team)
                rows_updated += 1
            except Exception as e:
                self.message_user(request, str(e), messages.ERROR)
        msg = _pl('{count} team payment created',
                  '{count} team payments created',
                  rows_updated).format(count=rows_updated)
        self.message_user(request, msg)

    create_payment.short_description = _('Create payment for selected')

    def remove_payment(self, request, queryset):
        rows_updated = 0
        for team in queryset.all():
            try:
                processor.remove_team_payment(team)
                rows_updated += 1
            except Exception as e:
                self.message_user(request, str(e), messages.ERROR)
        msg = _pl('{count} team payment removed',
                  '{count} team payments removed',
                  rows_updated).format(count=rows_updated)
        self.message_user(request, msg)

    remove_payment.short_description = _('Remove payment from selected')

    def make_paid(self, request, queryset):
        rows_updated = 0
        for team in queryset.all():
            try:
                processor.mark_team_paid(team)
                processor.send_payment_received(team)
                rows_updated += 1
            except Exception as e:
                self.message_user(request, str(e), messages.ERROR)
        self.create_message(request, rows_updated, _('marked'))

    make_paid.short_description = _("Mark selected as paid")

    def make_unpaid(self, request, queryset):
        rows_updated = 0
        for team in queryset.all():
            try:
                processor.mark_team_unpaid(team)
                rows_updated += 1
            except ValueError as e:
                self.message_user(request, str(e), messages.ERROR)
        self.create_message(request, rows_updated, _('unmarked'))

    make_unpaid.short_description = _("Mark selected as unpaid")

    def create_message(self, request, updated, mark):
        msg = _pl('%(count)d team was successfully %(mark)s as paid',
                  '%(count)d teams were successfully %(mark)s as paid',
                  updated) % {'count': updated, 'mark': mark}
        self.message_user(request, msg)


admin.site.register(models.Team, TeamAdmin)


class TeamInviteAdmin(admin.ModelAdmin):
    list_display = ('email', 'team_link', 'status', 'user_link')
    list_filter = ('status',)

    def team_link(self, obj):
        return mark_safe('<a href="{}">{}</a>'.format(
            reverse('admin:teams_team_change', args=(obj.team.pk,)),
            obj.team
        ))
    team_link.short_description = _('Team')

    def user_link(self, obj):
        if obj.user is not None:
            return mark_safe('<a href="{}">{}</a>'.format(
                reverse('admin:auth_user_change', args=(obj.user.pk,)),
                obj.user
            ))
        else:
            return '-'
    user_link.short_description = _('User')


admin.site.register(models.TeamInvite, TeamInviteAdmin)

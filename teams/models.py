from datetime import datetime

from autoslug import AutoSlugField
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.db import models, connection
from django.utils.translation import ugettext_lazy as _
from profiles.models import (PlayerProfile,
                             UserProfile)
from ygames.utils import generate_code

from .consts import (TEAM_CREATED,
                     TEAM_PAYMENT_CREATED,
                     TEAM_PAYMENT_PAID,
                     MEMBER_PLAYER,
                     MEMBER_SUBSTITUTE)

_PLAYER_STATUSES = (
    (MEMBER_PLAYER, _('Player')),
    (MEMBER_SUBSTITUTE, _('Substitute')),
)


class TeamManager(models.Manager):
    def create_my_team(self, name, acronym, admin, game_rules):
        team = self.create(
            name=name,
            acronym=acronym,
            game_rules=game_rules
        )
        team.admins.add(admin)
        team.save()
        return team

    def get_email(self, team):
        emails = {m.player.user.email for m in team.members.all()}
        emails += {i.email or i.user.email for i in team.invites.all()}
        emails += {a.email for a in team.admins.all()}
        return list(emails)

    def estimate_size(self, team):
        return len(self.get_email(team))

    def filter_current(self):
        now = datetime.now()
        return self.filter(
            game_rules__tournament__registration_start__lte=now,
            game_rules__tournament__registration_end__gte=now
        )


def concat_slug(team):
    return '{} {}'.format(team.game_rules.slug, team.acronym or team.name)


class Team(models.Model):
    """Team model.

    Attributes:
        name (str): Name of team.
        acronym (str): Shortened name of team.
        admins (list of User): Users that are granted the privilege to
            manage this team.
        vip (bool): If tema is VIP. (don't ask me if this should be here).
        created (:obj:`datetime.datetime`) Date and time when team was created.
        game_rules (:obj:`game.models.GameRules`) Game (rule) to which is team
            registered. (I dont think this should be here either.)
    """
    _PAYMENT_STATES = [(TEAM_CREATED, _('Team created')),
                       (TEAM_PAYMENT_CREATED, _('Payment created')),
                       (TEAM_PAYMENT_PAID, _('Payment paid'))]

    name = models.CharField(max_length=50, db_index=True)
    acronym = models.CharField(max_length=10, db_index=True)

    admins = models.ManyToManyField(User)

    created = models.DateTimeField(null=False, auto_now_add=True)

    game_rules = models.ForeignKey('game.GameRules')

    slug = AutoSlugField(populate_from=concat_slug)

    objects = TeamManager()

    class Meta:
        unique_together = (('game_rules', 'name'), ('game_rules', 'acronym'))
        permissions = (
            ('can_overview', _('Can see teams overview')),
            ('can_payments', _('Can see teams payments')),
        )

    def __str__(self):
        return "[{}] {} ({})".format(self.acronym, self.name, self.game_rules)

    def get_vs(self):
        """Returns Varaible symbol for team.

        Returns:
            str: Generated variable symbol.
        """
        return '{:02d}{:02d}{:04d}'.format(self.game_rules.tournament.id,
                                           self.game_rules.id, self.id)

    def vip(self):
        return self.game_rules.vip

    def paid(self):
        """Returns wheter the paiments was received or not.

        Returns:
            bool: True if payment was received, otherwise False.
        """
        return self.paymentitem_set.filter(
            payment__pay_date__isnull=False).exists()

    paid.boolean = True

    def get_admins_emails(self):
        return ','.join((a.email for a in self.admins.all()))

    get_admins_emails.short_description = 'E-mail'

    def get_admins_phones(self):
        return ','.join((str(a.profile.phone_number or '')
                         for a in self.admins.all()))

    get_admins_phones.short_description = 'Phone'

    def get_emails_with_names(self):
        def _f_username(user, default=None):
            if user is None:
                return default
            return '[{}] {} {}'.format(user.username, user.first_name,
                                       user.last_name)

        emails = {m.player.user.email: _f_username(m.player.user)
                  for m in self.members.all()}
        emails.update({(i.email or i.user.email): _f_username(i.user, i.email)
                       for i in self.invites.all()})
        emails.update({a.email: _f_username(a) for a in self.admins.all()})
        return [(email, name) for email, name in emails.items()]

    def get_emails(self):
        emails = {m.player.user.email for m in self.members.all()}
        emails.update({i.email or i.user.email for i in self.invites.all()})
        emails.update({a.email for a in self.admins.all()})
        return list(emails)

    def estimate_size(self):
        return len(self.get_emails())


class TeamMember(models.Model):
    team = models.ForeignKey(Team, related_name='members')
    player = models.ForeignKey('profiles.PlayerProfile')

    status = models.CharField(max_length=1, choices=_PLAYER_STATUSES)

    def __str__(self):
        return '{} - {} MEM<{}>'.format(self.team.acronym, self.player,
                                        self.status)

    class Meta:
        unique_together = (('team', 'player'),)


class TeamInviteManager(models.Manager):
    """Manager object for TeamInvite models managment."""

    def invite_user(self, team, email, status='P'):
        """Creates new team invitation object.

        Arguments:
            email (str): Email address of invited user.
            team (Team): Team which invites user.
            status (str, optional): Player type if he is invited as substitute
                (`'S'`) or as player (`'P'`).

        Returns:
            TeamInvite: Team invitation model object.
        """
        code = generate_code(64)
        try:
            user = User.objects.get(email=email)
        except ObjectDoesNotExist:
            user = None
        return self.create(team=team, user=user, email=email, code=code,
                           status=status)


class TeamInvite(models.Model):
    """Team inviatation model object.

    Attributes:
        team (Team): Team where was certain player invited.
        status (str): Type of invitate, whether the player was invited as
            (`'S'`) player substitute or (`'P'`) as actual player.
        email (str): Invited user email address, this should be the same
            as email address used in user registration.
        code (str): Randomly generated code, used for team invitation
            verification.
        user (User, optional): Actual user model, if it exists.
    """
    team = models.ForeignKey(Team, related_name='invites')
    status = models.CharField(max_length=1, choices=_PLAYER_STATUSES)
    user = models.ForeignKey(User, null=True, blank=True)

    email = models.CharField(max_length=250, db_index=True, blank=True)

    code = models.CharField(max_length=64, blank=True, db_index=True)

    date = models.DateTimeField(auto_now_add=True)

    objects = TeamInviteManager()

    def user_has_player_profile(self):
        """Checks whether user has player profile for certain game type.

        Returns:
            bool: True if user has already player profile, otherwise False.
        """
        return PlayerProfile.objects.filter(
            user=self.user, game=self.team.game_rules.game).exists()

    def user_is_registered(self):
        """Returns whether is such user already registered.

        Returns:
            bool: True if user with such email already exists, otherwise False.
        """
        return self.user is not None and self.user.is_active

    def __str__(self):
        return '{} - {} INV<{}>'.format(
            self.team.acronym,
            self.user or self.email,
            self.status
        )


_VALID_ORDERINGS = ('name', 'emails', 'price', 'game', 'vip', 'has_paid',
                    'pay_date', 'team_created')


def load_teams(tournament_id=None, sort=None):
    with connection.cursor() as cursor:
        query = (
            "SELECT t.name as name, t.id as team_id,"
            " string_agg(DISTINCT au.email, ',') as emails,"
            " sum(pi.price) as price, concat(g.name, ' ', gr.name) as game,"
            " gr.id as game_rule_id, gr.vip as vip,"
            " p.pay_date IS NOT NULL as has_paid,"
            " p.pay_date as pay_date, t.created as team_created,"
            " gr.tournament_id as tournament_id"
            " FROM teams_team as t"
            " INNER JOIN teams_team_admins as ta"
            " ON t.id = ta.team_id"
            " INNER JOIN auth_user as au"
            " ON ta.user_id = au.id"
            " INNER JOIN game_gamerules as gr"
            " ON t.game_rules_id = gr.id"
            " INNER JOIN game_game as g"
            " ON gr.game_id = g.id"
            " LEFT JOIN payments_payment as p"
            " ON t.id = p.team_id"
            " LEFT JOIN payments_paymentitem as pi"
            " ON p.id = pi.payment_id"
            " GROUP BY t.name, t.id, g.name, gr.name, gr.vip, gr.id,"
            " p.pay_date, t.created, gr.tournament_id"
        )
        if tournament_id:
            query += " HAVING gr.tournament_id = %s"
            p = (tournament_id,)
        else:
            p = tuple()

        if sort and sort in _VALID_ORDERINGS:
            query += " ORDER BY {col} DESC NULLS LAST".format(col=sort)

        cursor.execute(query, params=p)
        columns = [col[0] for col in cursor.description]
        return [dict(zip(columns, row)) for row in cursor.fetchall()]

_VALID_PAYMENT_ORDERINGS = ('team_name', 'payer', 'game', 'payer_reference',
                            'player_price', 'accom_price',
                            'total_price', 'has_paid', 'pay_date',
                            'team_created', 'emails', 'phones')


def load_payments(tournament_id=None, sort=None):
    with connection.cursor() as cursor:
        query = (
            "SELECT t.name as team_name,"
            " concat(pu.first_name, ' ', pu.last_name) as payer,"
            " concat(g.name, ' ', gr.name) as game,"
            " p.transaction_id as payer_reference,"
            " SUM(CASE WHEN pi.team_id IS NULL THEN 0"
            " ELSE pi.price * pi.quantity END) as player_price,"
            " SUM(CASE WHEN pi.accommodation_id IS NULL AND"
            " pi.product_id IS NULL THEN 0"
            " ELSE pi.price * pi.quantity END) as accom_price,"
            " SUM(pi.price * pi.quantity) as total_price,"
            " p.pay_date IS NOT NULL as has_paid,"
            " p.pay_date as pay_date,"
            " t.created as team_created,"
            " string_agg(DISTINCT au.email, ',') as emails,"
            " string_agg(DISTINCT aup.phone_number, ',') as phones"
            " FROM payments_payment as p"
            " INNER JOIN payments_paymentitem as pi"
            " ON p.id = pi.payment_id"
            " INNER JOIN auth_user as pu"
            " ON p.payer_id = pu.id"
            " LEFT JOIN teams_team as t"
            " ON p.team_id = t.id"
            " LEFT JOIN game_gamerules as gr"
            " ON t.game_rules_id = gr.id"
            " LEFT JOIN game_game as g"
            " ON gr.game_id = g.id"
            " LEFT JOIN teams_team_admins as ta"
            " ON t.id = ta.team_id"
            " LEFT JOIN auth_user as au"
            " ON ta.user_id = au.id"
            " LEFT JOIN profiles_userprofile as aup"
            " ON au.id = aup.user_id"
            " GROUP BY t.name, p.transaction_id, pu.first_name, pu.last_name,"
            " g.name, gr.name, p.pay_date, t.created, gr.tournament_id"
        )
        if tournament_id:
            query += " HAVING gr.tournament_id = %s"
            p = (tournament_id,)
        else:
            p = tuple()

        if sort and sort in _VALID_PAYMENT_ORDERINGS:
            query += " ORDER BY {col} DESC NULLS LAST".format(col=sort)

        cursor.execute(query, params=p)
        columns = [col[0] for col in cursor.description]
        return [dict(zip(columns, row)) for row in cursor.fetchall()]


def _load_tournament_teams(tournament_id=None, limit=None, offset=None):
    with connection.cursor() as cursor:
        query = (
            "SELECT string_agg(a.email, ',') AS admin_emails,"
            " string_agg(ap.phone_number, ',') AS admin_phones,"
            " t.name AS team_name, t.id AS team_id,"
            " g.name AS game_name, gr.name AS game_rule_name,"
            " gr.slug AS game_rule_slug, gr.vip AS vip,"
            " gr.id AS game_rule_id, tu.name AS tournament_name,"
            " tu.slug AS tournament_slug, tu.id as tournament_id"
            " FROM teams_team AS t"
            " INNER JOIN teams_team_admins AS ta"
            " ON ta.team_id = t.id"
            " LEFT OUTER JOIN auth_user AS a"
            " ON a.id = ta.user_id"
            " INNER JOIN profiles_userprofile AS ap"
            " ON ap.user_id = a.id"
            " INNER JOIN game_gamerules AS gr"
            " ON gr.id = t.game_rules_id"
            " INNER JOIN game_game AS g"
            " ON g.id = gr.game_id"
            " INNER JOIN game_tournament AS tu"
            " ON tu.id = gr.tournament_id"
            " GROUP BY t.name, t.id, g.name, gr.name, gr.slug, gr.vip,"
            " gr.id, tu.name, tu.slug, tu.id"
        )
        if tournament_id is not None:
            query += " HAVING tu.id = %d"
        query += " ORDER BY t.id DESC"
        if limit is not None:
            query += " LIMIT %d"

        if offset is not None:
            query += " OFFSET %d"
        p = (int(e) for e in (tournament_id, limit, offset) if e is not None)
        cursor.execute(query, params=tuple(p))
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns, row))
                for row in cursor.fetchall()]
        return data

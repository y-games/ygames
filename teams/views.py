from datetime import datetime

import rules
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import (ObjectDoesNotExist,
                                    MultipleObjectsReturned)
from django.db.models import Q
from django.http import (HttpResponseNotFound,
                         HttpResponseNotAllowed)
from django.shortcuts import (render,
                              redirect, get_object_or_404)
from django.utils.translation import (ugettext_lazy as _,
                                      ungettext as _pl)
from django.views.decorators.http import require_GET, require_POST
from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
from rules.contrib.views import PermissionRequiredMixin, permission_required, \
    objectgetter
from unidecode import unidecode

from accommodations.models import AccommodationReservation
from game.models import (Tournament,
                         GameRules)
from include.mails import MailBuilder
from payments.models import PaymentItem
from profiles.models import PlayerProfile
from teams.forms import EditTeamForm
from teams.models import (Team,
                          TeamMember,
                          TeamInvite, load_teams, load_payments)
from teams.tables import TeamsTable, PaymentTable
from .consts import (TEAM_PAYMENT_CREATED,
                     TEAM_PAYMENT_PAID)


def send_invitation_email(invite, request):
    """Sends email to invited user.

    Args:
        invite (ygames.teams.models.TeamInvite): Invitation structure.
        request (django.http.request.HttpRequest): Invitation creating request.
    """
    builder = MailBuilder(to=invite.email,
                          subject=_('[Y-Games] Team invitation'),
                          text_link='email/invitation_email.txt',
                          html_link='email/invitation_email.html')
    builder.auto_generated = True
    email = builder.create({
        'team_name': invite.team.name,
        'request': request,
        'code': invite.code
    })
    email.send()


@login_required
def team_overview(request):
    user = request.user
    open_tournaments = Tournament.objects.filter_enabled_registration()
    closed_tournaments = Tournament.objects.filter(
        registration_end__lt=datetime.now())
    managed_teams = Team.objects.filter(
        Q(admins__pk=user.pk) | Q(members__player__user=user.pk),
        game_rules__tournament__in=open_tournaments).distinct()
    invites = TeamInvite.objects.filter(Q(user=user) | Q(email=user.email))
    admin_of = {team.pk for team in managed_teams
                if managed_teams.filter(admins=user).exists()}
    payment_created = {team.pk for team in managed_teams.filter(
        paymentitem__isnull=False)}

    return render(request, 'teams/overview.html',
                  {'open_tournaments': open_tournaments,
                   'closed_tournaments': closed_tournaments,
                   'administered': admin_of,
                   'payment_created': payment_created,
                   'teams': managed_teams,
                   'invites': invites})


@require_GET
@permission_required('teams.delete_teaminvite', objectgetter(TeamInvite))
def accept_invitation(request, pk):
    invitation = get_object_or_404(TeamInvite, pk=pk)
    player_profile = PlayerProfile.objects.get(
        user=request.user,
        game=invitation.team.game_rules.game)
    TeamMember.objects.create(team=invitation.team,
                              player=player_profile,
                              status=invitation.status)
    accs = AccommodationReservation.objects.filter_by_email(
        request.user.email, team=invitation.team)
    user_accs = AccommodationReservation.objects.filter_by_user(request.user)
    if accs.exists() and user_accs.exists():
        accs.delete()
        user_accs.update(team=invitation.team)
    invitation.delete()

    messages.success(request, _('Team invitation successfully accepted'))

    return redirect('teams_overview')


@require_GET
@permission_required('teams.delete_teaminvite', objectgetter(TeamInvite))
def decline_invitation(request, pk):
    invitation = get_object_or_404(TeamInvite, pk=pk)
    invitation.delete()
    messages.success(request, _('Team invitation was declined'))

    return redirect('teams_overview')


@login_required
def choose_game(request, pk):
    if request.method != 'GET':
        return HttpResponseNotAllowed(['GET'])
    try:
        tournament = Tournament.objects.get(pk=pk)
        games_bundle = []
        for game_rule in tournament.gamerules_set.all():
            games_bundle.append({
                "game_rule": game_rule,
                "name": game_rule.game.name,
                "player_count": game_rule.player_count,
                "capacity": game_rule.capacity,
                "registration_available": bool(game_rule.teams_left),
                "pk": game_rule.pk
            })
        return render(request, 'teams/choose_game.html',
                      {'tournament': tournament, 'game_bundle': games_bundle})

    except ObjectDoesNotExist:
        return HttpResponseNotFound


@login_required
@permission_required('teams.delete_team', fn=objectgetter(Team))
def remove_unpaid_team(request, pk):
    team = get_object_or_404(Team, pk=pk)

    if PaymentItem.objects.filter_by_billable(team).exists():
        messages.error(request, _('You can not delete team'
                                  ' with created payment.'))
    else:
        team.delete()
        messages.success(request,
                         _('Team {team} was deleted').format(team=team.name))

    return redirect('teams_overview')


class TeamRegisterView(LoginRequiredMixin, View):
    def get(self, request, pk):
        try:
            game_rules = GameRules.objects.get(pk=pk)
            price = game_rules.price + (game_rules.price_per_player *
                                        game_rules.player_count)
            form = EditTeamForm(game_rules)
            return render(request, 'teams/register.html',
                          {'form': form, 'gr': game_rules, 'price': price})
        except ObjectDoesNotExist:
            return HttpResponseNotFound()

    def post(self, request, pk):
        try:
            game_rules = GameRules.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return HttpResponseNotFound
        form = EditTeamForm(game_rules, request.POST)
        price = game_rules.price + (game_rules.price_per_player *
                                    game_rules.player_count)
        if not form.is_valid():
            return render(request, 'teams/register.html',
                          {'form': form, 'gr': game_rules, 'price': price})

        team = Team.objects.create_my_team(
            name=unidecode(form.cleaned_data['name']),
            acronym=unidecode(form.cleaned_data['acronym']),
            admin=request.user,
            game_rules=game_rules
        )

        for i in range(game_rules.player_count):
            email = form.cleaned_data['player_{}'.format(i + 1)]
            _invite_user(request, email, team)

        return redirect('teams_overview')


class TeamEditorView(SingleObjectMixin, PermissionRequiredMixin, View):
    permission_required = 'teams.change_team'
    model = Team

    def get(self, request, pk):
        try:
            team = self.get_object()

            count = (TeamInvite.objects.filter(team=team).count() +
                     TeamMember.objects.filter(team=team).count())
            # player count and one substitute
            can_invite = team.game_rules.player_count + 1 > count

            return render(request, 'teams/edit.html',
                          {'team': team, 'can_invite': can_invite})

        except ObjectDoesNotExist:
            return HttpResponseNotFound(_("Team does not exists"))

    def post(self, request, pk):
        team = self.get_object()

        action = request.POST.get('action', False)

        if action == 'remove_member':
            member_id = request.POST.get('member')
            return self._remove_member(member_id)

        if action == 'cancel_invite':
            invitation_id = request.POST.get('invite')
            return self._cancel_invitation(invitation_id)

        if action == 'invite':
            email = request.POST.get('email')
            return self._invite(email)

        return redirect('teams_edit', team.pk)

    def _remove_member(self, member_id):
        team = self.get_object()
        member = TeamMember.objects.get(id=member_id, team=team)
        AccommodationReservation.objects.filter_by_user(
            member.player.user,
            team=team,
            paymentitem__payment__pay_date__isnull=False,
            paymentitem__payment__payer__in=team.admins.all()
        ).update(user=None)
        d_items, row_count = TeamMember.objects.filter(id=member_id,
                                                       team=team).delete()
        deleted = row_count['teams.TeamMember']
        if deleted == 0:
            messages.error(self.request, _('Team member does not exist'))
        else:
            messages.success(self.request,
                             _pl('Successfully removed %(count)d team member',
                                 'Successfully removed %(count)d team members',
                                 deleted).format(count=deleted))
        return redirect('teams_edit', team.pk)

    def _cancel_invitation(self, invitation_id):
        team = self.get_object()
        d, count = TeamInvite.objects.filter(id=invitation_id,
                                             team=team).delete()
        canceled = count['teams.TeamInvite']
        if canceled == 0:
            messages.error(self.request, _('Invitation does not exist'))
        else:
            messages.success(self.request,
                             _pl('Successfully removed %(count)d invitation',
                                 'Successfully removed %(count)d invitations',
                                 canceled) % {'count': canceled})
        return redirect('teams_edit', team.pk)

    def _invite(self, email):
        team = self.get_object()
        if not _can_invite_player(team):
            messages.add_message(self.request, messages.WARNING,
                                 _("Maximum number of players reached."))
            return redirect('teams_edit', team.id)

        _invite_user(self.request, email, team)
        return redirect('teams_edit', team.pk)


@require_GET
@permission_required('teams.delete_teaminvite', fn=objectgetter(TeamInvite))
def cancel_invitation(request, pk):
    pass


@require_POST
@permission_required('teams.add_teaminvite', fn=objectgetter(Team))
def invite_player(request, pk):
    team = get_object_or_404(Team, pk=pk)
    if not _can_invite_player(team):
        messages.warning(request, _("Maximal number of players reached"))
        return redirect('teams_edit', team.pk)
    email = request.POST.get('email', None)
    email = email.strip()
    if not email:
        messages.error(request, _('No email specified'))
        return redirect('teams_edi', team.pk)
    _invite_user(request, email, team)
    return redirect('teams_edit', team.pk)


def _invite_user(request, email, team):
    invitation = TeamInvite.objects.invite_user(team, email)

    if not invitation.user_is_registered():
        send_invitation_email(invitation, request)


def _can_invite_player(team):
    actual_player_count = (TeamInvite.objects.filter(team=team).count() +
                           TeamMember.objects.filter(team=team).count())
    max_players = team.game_rules.player_count + 1

    return actual_player_count < max_players


@require_GET
@login_required
def show_team_detail(request, pk):
    team = get_object_or_404(Team, pk=pk)
    admin_qs = team.admins.filter(pk=request.user.pk)
    is_admin = admin_qs.exists()
    is_paid = team.paymentitem_set.filter(
        payment__pay_date__isnull=False).exists()
    is_current = team.game_rules.tournament.is_registration_active()
    context = {'team': team, 'is_admin': is_admin, 'is_paid': is_paid,
               'is_current': is_current}
    return render(request, 'teams/detail.html', context=context)


class ShowTeams(View):

    permission_denied_message = _('Insufficient rights to see team'
                                  ' information')

    raise_exception = True

    def get(self, request):
        # TODO zmenit na viacero turnajov
        sort = request.GET.get('sort', None)
        tournament = Tournament.objects.last()
        team_data_lst = load_teams(tournament_id=tournament.id, sort=sort)

        def _transform(team_data):
            return {
                'name': team_data['name'],
                'emails': team_data['emails'],
                'game': team_data['game'],
                'vip': team_data['vip'],
                'payer_reference': '{:02d}{:02d}{:04d}'.format(
                    team_data['tournament_id'],
                    team_data['game_rule_id'],
                    team_data['team_id']),
                'has_paid': team_data['has_paid'],
                'price': team_data['price'],
                'pay_date': team_data['pay_date'],
                'team_created': team_data['team_created']
            }

        table = TeamsTable([_transform(data) for data in team_data_lst])
        return render(request, 'teams/game_team_overview.html',
                      {'table': table})


class ShowPayments(PermissionRequiredMixin, View):
    permission_required = 'teams.can_payments'

    permission_denied_message = _('Insufficient rights to see team payments')

    raise_exception = True

    def get(self, request):
        sort = request.GET.get('sort', None)
        tournament = Tournament.objects.filter_enabled_registration().last()
        payment_data = load_payments(tournament_id=tournament.id, sort=sort)
        table = PaymentTable(payment_data)

        # TODO tournament choice
        return render(request, 'teams/payments_overview.html',
                      {'table': table})

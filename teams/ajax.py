import json

from django.contrib.auth.decorators import (login_required,
                                            permission_required)
from django.http import HttpResponse
from django.utils.translation import ugettext as _
from django.views.decorators.http import require_POST, require_GET
from django_ajax.decorators import ajax

from game.models import Tournament
from profiles.utils import create_valid_response
from teams.models import Team, _load_tournament_teams


@ajax
@require_POST
def register_validation(req, pk):
    field = req.POST['name']
    value = req.POST['value']

    if field == 'name' and Team.objects.filter(game_rules__pk=pk,
                                               name=value).exists():
        return create_valid_response(False,
                                     _("This team name is already in use"))

    if field == 'acronym' and Team.objects.filter(game_rules__pk=pk,
                                                  acronym=value).exists():
        return create_valid_response(False,
                                     _("This team acronym is already in use"))

    return create_valid_response(True)


@require_GET
@login_required
@permission_required('teams.can_overview', raise_exception=True)
def load_teams_table(request, pk):
    print('Starting load teams')
    if pk == 'last':
        tournament = Tournament.objects.last().pk
    elif pk == 'all':
        tournament = None
    else:
        tournament = pk
    limit = request.GET['limit'] if 'limit' in request.GET else None
    page = request.GET['page'] if 'page' in request.GET else None

    offset = None
    if limit and page:
        limit = int(limit)
        page = int(page)
        offset = limit * (page - 1)

    team_data = _load_tournament_teams(tournament, limit, offset)
    return HttpResponse(json.dumps(team_data), content_type='application/json')



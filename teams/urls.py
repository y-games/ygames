from django.conf.urls import url

from . import (views,
               ajax)

urlpatterns = [
    url(r'^$', views.team_overview, name='teams_overview'),
    url(r'^overview/$', views.ShowTeams.as_view(), name='show_teams'),
    url(r'^payments/$', views.ShowPayments.as_view(), name='show_payments'),
    url(r'^invite/(?P<pk>\d+)/accept/', views.accept_invitation,
        name='accept_invite'),
    url(r'^invite/(?P<pk>\d+)/decline/', views.decline_invitation,
        name='decline_invite'),
    url(r'^register/_validate/(?P<pk>\d+)/$',
        view=ajax.register_validation, name='teams_validator'),
    url(r'^register/game/(?P<pk>\d+)/$',
        views.TeamRegisterView.as_view(), name='teams_register'),
    url(r'^register/(?P<pk>\d+)/$', views.choose_game,
        name='teams_choose_game'),
    url(r'^(?P<pk>\d+)/$',
        views.show_team_detail, name='teams_detail'),
    url(r'^delete/(?P<pk>\d+)/', views.remove_unpaid_team, name='team_delete'),
    url(r'^edit/(?P<pk>\d+)/$', views.TeamEditorView.as_view(),
        name='teams_edit'),
    # url(r'^overview/(?P<pk>\d+|all|last)/_data/$', ajax.load_teams_table)
]

import rules


@rules.predicate
def is_team_admin(user, team):
    return team.admins.filter(pk=user.pk).exists()


@rules.predicate
def is_my_invitation(user, invitation):
    return user.email == invitation.email or user == invitation.user


@rules.predicate
def is_inviting_team_admin(user, invitation):
    if invitation.team is None:
        return False
    return invitation.team.admins.filter(pk=user.pk).exists()


rules.add_perm('teams.add_team', rules.always_allow)
rules.add_perm('teams.change_team', is_team_admin)
rules.add_perm('teams.delete_team', is_team_admin | rules.is_superuser)

rules.add_perm('teams.add_teaminvite', is_team_admin)
rules.add_perm('teams.change_teaminvite', rules.always_deny)
rules.add_perm('teams.delete_teaminvite',
               is_inviting_team_admin | is_my_invitation)

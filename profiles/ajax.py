from django.contrib.auth.models import User
from django.http import HttpResponseNotAllowed
from django_ajax.decorators import ajax
from django.utils.translation import ugettext as _
from unidecode import unidecode

from profiles.utils import create_valid_response


@ajax
def register_validation(request):
    if request.method != 'POST':
        raise HttpResponseNotAllowed(['POST'])
    field = request.POST['name']
    value = request.POST['value']

    if field == 'username':
        username = unidecode(value)
        if User.objects.filter(username=username).exists():
            return create_valid_response(
                False,
                _("User with username %s already exists!") % username)
        return create_valid_response(True)
    if field == 'email':
        if User.objects.filter(email=value).exists():
            return create_valid_response(
                False,
                _("User with same email address already exists! Did you"
                  " forgot your password?"))
        return create_valid_response(True)
    return create_valid_response(False)

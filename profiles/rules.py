"""Default user and user profile django permissions."""
import rules


@rules.predicate()
def is_account_owner(user, visited_account):
    """Checks whether currently visited account is his/hers own.

    Args:
        user (django.contrib.auth.models.User): Visitor user model.
        visited_account (ygames.profiles.models.User): Visited user
            account.
    Returns:
        bool: True iff visitor is profile owner.
    """
    if not visited_account:
        return False
    return user.pk == visited_account.pk


@rules.predicate()
def is_staff_member(user):
    """Checks whether visitor is a staff member.

    Args:
        user (django.contrib.auth.models.User): Visitor model.

    Returns:
        bool: True if visitor is staff member, otherwise False.
    """
    return user.is_staff


@rules.predicate()
def is_superuser(user):
    """Checks whether current visitor is super user.

    Args:
        user (django.contrib.auth.models.User): Visitor user model.

    Returns:
        bool: True if visitor is superuser.
    """
    return user.is_superuser


rules.add_perm('profiles', rules.always_allow)

rules.add_perm('profiles.change_userprofile',
               is_account_owner | is_staff_member)
rules.add_perm('profiles.delete_userprofile', is_superuser)
rules.add_perm('profiles.add_userprofile', rules.always_allow)

rules.add_perm('profiles.add_playerprofile', is_account_owner)
rules.add_perm('profiles.change_playerprofile',
               is_account_owner | is_staff_member)
rules.add_perm('profiles.delete_playerprofile',
               is_account_owner | is_superuser)


from django.conf.urls import url

from . import (views,
               ajax)


urlpatterns = [
    url(r'^activate/(?P<username>[\w.-]+)/(?P<code>[\w-]+)/$',
        view=views.activation, name='activate'),
    url(r'^generate_activation/(?P<username>[\w.-]+)/$',
        view=views.generate_activation, name='generate_activation'),
    url(r'^register/$', view=views.RegistrationView.as_view(),
        name='register'),
    url(r'register/_validate/$', view=ajax.register_validation,
        name='profile_validator'),
    url(r'^(?P<username>[\w.-]+)$', views.view_profile, name='view_profile'),
    url(r'^edit/(?P<pk>\d+)/$',
        views.UserProfileEditorView.as_view(), name='edit_profile'),
    url(r'^(?P<username>[\w.-]+)/player/add/$',
        views.PlayerProfileAddView.as_view(), name='add_player_profile'),
    url(r'^(?P<username>[\w.-]+)/player/edit/(?P<game>\w+)/$',
        views.PlayerProfileEditorView.as_view(), name='edit_player_profile'),
]

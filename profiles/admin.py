from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.auth.models import User
from django.db.models import Q
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from rules.contrib.admin import (ObjectPermissionsModelAdmin,
                                 ObjectPermissionsStackedInline)

from game.models import Tournament
from include.actions import export_as_csv_action
from profiles import models


class UserProfileInline(ObjectPermissionsStackedInline):
    model = models.UserProfile
    fk_name = 'user'


class ProfiledUserAdmin(ObjectPermissionsModelAdmin):

    def profile__ynet_id(self, obj):
        return obj.profile.ynet_id

    list_select_related = True
    inlines = [
        UserProfileInline,
    ]
    list_filter = ('date_joined', 'is_staff', 'is_active', 'is_superuser')
    list_display = ('username', 'first_name', 'last_name', 'email', 'is_staff',
                    'date_joined', 'profile__ynet_id')
    search_fields = ['username', 'email', 'first_name', 'last_name']
    date_hierarchy = 'date_joined'
    list_max_show_all = 1000
    actions = [export_as_csv_action("Export Y-net IDs",
                                    fields=['username', 'profile__ynet_id'])]

admin.site.unregister(User)
admin.site.register(User, ProfiledUserAdmin)


class TournamentFilter(SimpleListFilter):
    title = _('Tournament')
    parameter_name = 'tournament'

    def lookups(self, request, model_admin):
        tourns = Tournament.objects.all().order_by('-pk')
        return [(t.id, t.name) for t in tourns]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(
                teammember__team__game_rules__tournament=self.value())
        else:
            return queryset


class InTeamFilter(SimpleListFilter):
    title = _('Is in Team')
    parameter_name = 'teamstate'

    def lookups(self, request, model_admin):
        return [
            ('i', _('In team')),
            ('n', _('Never in team')),
            ('c', _('Not currently in team'))
        ]

    def queryset(self, request, queryset):
        if self.value() == 'i':
            return queryset.filter(
                teammember__isnull=False,
            )
        if self.value() == 'n':
            return queryset.filter(
                teammember__isnull=True,
            )
        if self.value() == 'c':
            enab = Tournament.objects.filter_enabled_registration()
            return queryset.filter(
                ~Q(teammember__team__game_rules__tournament__in=enab)
            )
        return queryset


class PlayerProfileAdmin(ObjectPermissionsModelAdmin):
    list_display = ('game', 'nick', 'user_link', 'email', 'in_team')
    list_filter = ('game', TournamentFilter, InTeamFilter)
    search_fields = ['nick', 'user__username', 'user__first_name',
                     'user__last_name']
    actions = [export_as_csv_action(fields=['game', 'nick', 'user',
                                            'user__email', 'in_team'])]

    def in_team(self, obj):
        enab = Tournament.objects.filter_enabled_registration()
        qs = obj.teammember_set.filter(team__game_rules__tournament__in=enab)

        def _team_link(team):
            return mark_safe('<a href="{}">{}</a>'.format(
                reverse('admin:teams_team_change', args=(team.pk,)),
                team.name
            ))

        return mark_safe(','.join((_team_link(tm.team) for tm in qs))) or '-'
    in_team.short_description = _('In Team')

    def user_link(self, obj):
        return mark_safe('<a href="{}">{}</a>'.format(
            reverse('admin:auth_user_change', args=(obj.user.pk,)),
            obj.user
        ))
    user_link.short_description = _('User')

    def email(self, obj):
        return obj.user.email or '-'


admin.site.register(models.PlayerProfile, PlayerProfileAdmin)

from datetime import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Div
from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.forms import ModelForm
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django_countries import countries
from django_countries.fields import LazyTypedChoiceField
from django_countries.widgets import CountrySelectWidget
from phonenumber_field.formfields import PhoneNumberField
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm

from profiles import models
from profiles.widgets import BootstrapPhoneNumberWidget


class CredentialsForm(forms.Form):
    email = forms.EmailField(max_length=100, required=True)
    username = forms.CharField(max_length=50, required=True)
    password = forms.CharField(max_length=100, widget=forms.PasswordInput)
    verify_password = forms.CharField(max_length=100,
                                      widget=forms.PasswordInput)


class PersonalInfoFomr(forms.Form):
    first_name = forms.CharField(max_length=50, required=True)
    last_name = forms.CharField(max_length=50, required=True)
    country = LazyTypedChoiceField(choices=countries,
                                   required=True,
                                   widget=CountrySelectWidget,
                                   label=_('Country'))
    region = forms.CharField(max_length=100, required=True)
    gender = forms.ChoiceField(choices=models.GENDERS, required=True,
                               label=_('Gender'))
    birth_date = forms.CharField(required=True,
                                 widget=forms.DateInput(attrs={
                                     'id': 'datepicker'
                                 }))
    occupation = forms.CharField(required=True)


class RegistrationForm(forms.Form):
    first_name = forms.CharField(max_length=100, required=True,
                                 widget=forms.widgets.TextInput(attrs={
                                     'data-validation': 'required',
                                     'data-sanitize': 'trim'
                                 }),
                                 label=_('First name'))
    last_name = forms.CharField(max_length=100, required=True,
                                widget=forms.widgets.TextInput(attrs={
                                    'data-validation': 'required',
                                    'data-sanitize': 'trim'
                                }),
                                label=_('Last name'))
    username = forms.CharField()

    email = forms.EmailField()
    password = forms.CharField(min_length=8, max_length=100,
                               required=True,
                               help_text=_('Password must be at least 8'
                                           ' characters long'),
                               widget=forms.PasswordInput(attrs={
                                   'data-validation': 'required length',
                                   'data-validation-length': 'min8'
                               }),
                               label=_('Password'))
    password_again = forms.CharField(min_length=8, max_length=100,
                                     required=True,
                                     help_text=_('Retype your password'),
                                     widget=forms.PasswordInput(attrs={
                                         'data-validation': 'required confirmation',
                                         'data-validation-confirm': 'password'
                                     }),
                                     label=_('Repeat password'))
    country = LazyTypedChoiceField(choices=countries,
                                   widget=CountrySelectWidget(),
                                   label=_('Country'))
    state = forms.CharField(max_length=100, required=True, label=_('Region'),
                            widget=forms.widgets.TextInput(attrs={
                                'ajax-url': '/territories/load/',
                            }))
    city = forms.CharField(max_length=100, required=True, label=_('City'))
    street = forms.CharField(max_length=100, required=True, label=_('Street'),
                             help_text=_('If your city doesn\'t have'
                                         ' streets just type the city'
                                         ' name again'))
    zip_code = forms.CharField(max_length=100, required=True, label=_('ZIP'))

    gender = forms.ChoiceField(choices=models.GENDERS, required=True,
                               label=_('Gender'))
    birth_date = forms.CharField(required=True, label=_("Day of birth"),
                                 widget=forms.widgets.DateInput(attrs={
                                     'id': 'datepicker',
                                 }))
    school = forms.CharField(max_length=100, required=False,
                             label=_('School name'))
    school_year = forms.IntegerField(min_value=1, required=False,
                                     widget=forms.widgets.TextInput(attrs={
                                         'data-validation': 'number',
                                         'data-validation-optional': 'true'
                                     }), label=_('School year'))

    status = forms.ChoiceField(choices=models.STATUSES, required=True,
                               label=_('Occupation'))

    phone_number = PhoneNumberField(required=True, label=_('Phone number'),
                                    widget=BootstrapPhoneNumberWidget)

    ynet_id_validator = RegexValidator(regex=r'1\d{4}', message=_(
        'You entered invalid Ynet ID'))
    ynet_id = forms.CharField(max_length=50, required=False,
                              label=_('Ynet ID'),
                              help_text=_('Only Ynet members'),
                              validators=[ynet_id_validator])

    code = forms.CharField(widget=forms.HiddenInput, required=False)

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['username'] = forms.CharField(
            max_length=50, required=True,
            widget=forms.TextInput(
                attrs={
                    'data-validation': 'required alphanumeric ajax_validator',
                    'data-validation-allowing': "-_",
                    'ajax-validator-url': reverse('profile_validator')
                }
            ),
            label=_('Username'),
            help_text=_('Can contain only alphanumeric characters,'
                        ' dashes and underscores')
        )
        self.fields['email'] = forms.EmailField(
            max_length=100, required=True,
            widget=forms.widgets.EmailInput(attrs={
                'data-validation': 'email ajax_validator',
                'ajax-validator-url': reverse('profile_validator')
            }),
            label=_('Email'))
        self.helper = FormHelper()
        self.helper.field_class = ''
        self.helper.label_class = ''
        self.helper.field_class = ''
        self.helper.form_tag = False
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.html5_required = True
        self.helper.help_text_inline = True
        self.helper.error_text_inline = True
        self.helper.layout = Layout(
            Div(
                Div(
                    Fieldset(
                        _('Contact information'),
                        _('first_name'),
                        'last_name',
                        'email',
                        'country',
                        'state',
                        'city',
                        'street',
                        'zip_code',
                        'phone_number',
                    ),
                    css_class = "well well-lg col-lg-offset-1 col-md-offset-0 col-lg-3 col-md-4 col-sm-offset-1 col-sm-10 col-xs-12",
                ),
                Div(
                    Fieldset(
                        _('Credentials'),
                        'username',
                        'password',
                        'password_again',
                    ),
                    css_class = "well well-lg col-lg-offset-1 col-lg-2 col-md-offset-0 col-md-4 col-sm-offset-1 col-sm-10 col-xs-12",
                ),
                Div(
                    Fieldset(
                        _('Personal information'),
                        'birth_date',
                        'gender',
                        'status',
                        'school',
                        'school_year',
                        'code',
                        'ynet_id',
                    ),
                    css_class = "well well-lg col-lg-offset-1 col-lg-3 col-md-offset-0 col-md-4 col-sm-offset-1 col-sm-10 col-xs-12",
                ),
                css_class = "container-fluid",
            ),
        )

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()

        if User.objects.filter(username=cleaned_data['username']).exists():
            raise forms.ValidationError(
                {'username': [_("Username is already taken!"), ]})

        if cleaned_data.get("password") != cleaned_data.get("password_again"):
            raise forms.ValidationError({'password_again': [
                _("Password and Repeat password have to be the same!"), ]})

        if not cleaned_data['country'] in countries:
            raise forms.ValidationError(
                {'country': [_("You selected unknown country!"), ]})

        if cleaned_data['status'] == 's':

            if 'school' not in cleaned_data:
                raise forms.ValidationError(
                    {'school': [_("School field can not be blank")]})

            if 'school' in cleaned_data:
                if not cleaned_data['school']:
                    raise forms.ValidationError(
                        {'school': [_("School field can not be blank")]})

        if self.cleaned_data['status'] == 'w':
            if 'school' in self.errors:
                del self._errors['school']

            if 'school_year' in self.errors:
                del self._errors['school_year']

            self.cleaned_data['school'] = ''
            self.cleaned_data['school_year'] = None

        if User.objects.filter(email=cleaned_data['email']).exists():
            raise forms.ValidationError(
                {'email': [_('Email is already taken')]})

        return cleaned_data


class EditUserForm(ModelForm):
    first_name = forms.CharField(max_length=100, required=True,
                                 widget=forms.widgets.TextInput(attrs={
                                     'data-validation': 'required',
                                     'data-sanitize': 'trim'
                                 }),
                                 label=_('First name'))

    last_name = forms.CharField(max_length=100, required=True,
                                widget=forms.widgets.TextInput(attrs={
                                    'data-validation': 'required',
                                    'data-sanitize': 'trim'
                                }),
                                label=_('Last name'))
    email = forms.EmailField(widget=forms.EmailInput(attrs={"readonly": True}),
                             label=_('Email'),
                             help_text=_(
                                 'This field is only for informative purposes')
                             )

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(EditUserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.field_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2 col-sm-3'
        self.helper.field_class = 'col-lg-8 col-sm-9'
        self.helper.form_tag = False
        self.helper.form_method = 'post'
        self.helper.form_action = '?'
        self.helper.html5_required = True
        self.helper.help_text_inline = True
        self.helper.error_text_inline = True
        self.helper.layout = Layout(
            Fieldset(
                _('Contact information'),
                'first_name',
                'last_name',
                'email',
            ),
        )


class PassResetForm(PasswordResetForm):
    email = forms.EmailField(label=_('Email'), max_length=254, required=True,
                             widget=forms.EmailInput(
                                 attrs={
                                     'class': 'form-control',
                                     'placeholder': 'you@mailbox.com',
                                 }))


class SetPassForm(SetPasswordForm):
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput(attrs={
            'class': 'form-control'
        }),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        strip=False,
        widget=forms.PasswordInput(attrs={
            'class': 'form-control'
        }),
    )


class EditUserProfileForm(ModelForm):
    country = LazyTypedChoiceField(choices=countries,
                                   widget=CountrySelectWidget(),
                                   label=_('Country'))
    state = forms.CharField(max_length=100, required=True, label=_('Region'),
                            widget=forms.widgets.TextInput(attrs={
                                'ajax-url': '/territories/load/',
                            }))
    city = forms.CharField(max_length=100, required=True, label=_('City'))
    street = forms.CharField(max_length=100, required=True, label=_('Street'),
                             help_text=_('If youre city/village does not have'
                                         ' streets, please write here'
                                         ' city/vilage name.'))
    zip_code = forms.CharField(max_length=100, required=True, label=_('ZIP'))

    gender = forms.ChoiceField(choices=models.GENDERS, required=True,
                               label=_('Gender'))
    birth_date = forms.CharField(required=True,
                                 widget=forms.widgets.DateInput(attrs={
                                     'id': 'datepicker',
                                     'data-validation': 'birthdate',
                                     'data-validation-format': 'dd.mm.yyyy'
                                 }), label=_('Day of birth'))
    school = forms.CharField(max_length=100, required=False,
                             label=_('School name'))
    school_year = forms.IntegerField(min_value=1, required=False,
                                     widget=forms.widgets.TextInput(attrs={
                                         'data-validation': 'number',
                                         'data-validation-optional': 'true'
                                     }), label=_('School year'))

    status = forms.ChoiceField(choices=models.STATUSES, required=True,
                               label=_('Occupation'))

    phone_number = PhoneNumberField(required=False, label=_('Phone number'),
                                    widget=BootstrapPhoneNumberWidget)
    ynet_id_validator = RegexValidator(regex=r'1\d{4}', message=_(
        'You have entered invalid Ynet ID'))
    ynet_id = forms.CharField(max_length=50, required=False,
                              label=_('Ynet ID'),
                              validators=[ynet_id_validator],
                              help_text=_('Only for Ynet members'))

    class Meta:
        model = models.UserProfile
        fields = ['country', 'state', 'gender', 'birth_date', 'school',
                  'school_year', 'status', 'phone_number',
                  'ynet_id']

    def clean_birth_date(self):
        return datetime.strptime(self.cleaned_data['birth_date'],
                                 "%d.%m.%Y").date()

    def __init__(self, *args, **kwargs):
        super(EditUserProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.field_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2 col-sm-3'
        self.helper.field_class = 'col-lg-8 col-sm-9'
        self.helper.form_tag = False
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.html5_required = True
        self.helper.help_text_inline = True
        self.helper.error_text_inline = True
        self.helper.layout = Layout(
            Fieldset(
                _('Contact information'),
                'country',
                'state',
                'city',
                'street',
                'zip_code',
                'phone_number',
            ),
            Fieldset(
                _('Personal information'),
                'birth_date',
                'gender',
                'status',
                'school',
                'school_year',
                'ynet_id'
            ),
        )


class PlayerProfileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PlayerProfileForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['game'].widget.attrs['readonly'] = True

    class Meta:
        model = models.PlayerProfile
        fields = ['game', 'nick']

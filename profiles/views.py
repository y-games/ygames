from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import (PermissionDenied,
                                    ObjectDoesNotExist)
from django.db import IntegrityError
from django.db.models import Q
from django.http import (HttpResponseNotAllowed,
                         HttpResponseNotFound)
from django.shortcuts import (render,
                              redirect,
                              get_object_or_404)
from django.utils.translation import ugettext as _
from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
from unidecode import unidecode

from accommodations.models import AccommodationReservation
from game.models import Game
from include.mails import MailBuilder
from profiles.forms import (RegistrationForm,
                            EditUserForm,
                            EditUserProfileForm,
                            PlayerProfileForm)
from profiles.models import (UserProfile,
                             PlayerProfile)
from rules.contrib.views import PermissionRequiredMixin
from teams.models import TeamInvite
from ygames.utils import generate_code


def activation(request, username, code):
    if request.method != 'GET':
        return HttpResponseNotAllowed(['GET'])
    if not User.objects.filter(username=username).exists():
        return HttpResponseNotFound()
    user = User.objects.get(username=username)
    if user.is_active:
        return render(request, 'profiles/already_active.html')
    if user.profile.code_expiration <= datetime.now().date():
        return render(request, 'profiles/invalid_activation_code.html',
                      context={
                          'expired': True,
                          'username': user.username
                      })
    if user.profile.code != code:
        return render(request, 'profiles/invalid_activation_code.html',
                      context={
                          'expired': False,
                          'username': user.username
                      })
    user.is_active = True
    user.save()
    user.profile.code = ''
    user.profile.code_expiration = None
    user.profile.save()
    return render(request, 'profiles/successfully_activated.html')


def generate_activation(request, username):
    if request.method != 'GET':
        return HttpResponseNotAllowed(['GET'])
    if not User.objects.filter(username=username).exists():
        return HttpResponseNotFound()
    user = User.objects.get(username=username)
    if user.is_active:
        return render(request, 'profiles/already_active.html')
    user.profile.slug = generate_code()
    user.profile.slug_expiration = (datetime.now() +
                                    settings.ACTIVATION_TIME_OFFSET)
    user.profile.save()

    send_activation_mail(user, request)

    return render(request, 'profiles/post_register.html',
                  {'email': user.email})


@login_required
def view_profile(req, username):
    user = get_object_or_404(User, username=username)

    own_profile = False  # whether edit should be shown
    if req.user.is_authenticated() and req.user.pk == user.pk:
        own_profile = True

    try:
        p = PlayerProfile.objects.filter(user=user)
    except ObjectDoesNotExist:  # nie je to hrac
        p = None

    return render(req, 'profiles/view.html',
                  context={'profile_user': user,
                           'user': req.user,
                           'profiles_player': p,
                           'own_profile': own_profile})


def send_activation_mail(user, request):
    mail_builder = MailBuilder(to=user.email,
                               text_link='email/register_plain.txt',
                               subject=_('[Y-Games] Activation email'),
                               html_link='email/register_html.html')

    mail = mail_builder.create({
        'request': request,
        'username': user.username,
        'code': user.profile.code
    })
    mail.send()


class RegistrationView(View):
    def get(self, request):
        if request.GET.get('code'):
            try:
                team_invitation = TeamInvite.objects.get(
                    code=request.GET.get('code'), user=None)
                form = RegistrationForm(
                    initial={'code': team_invitation.code,
                             'email': team_invitation.email})
            except:
                raise PermissionDenied
        else:
            form = RegistrationForm()
        return render(request, 'auth/register.html', {'form': form})

    def post(self, request):
        form = RegistrationForm(request.POST)
        if not form.is_valid():
            return render(request, 'auth/register.html', {'form': form})
        data = form.cleaned_data
        code = data.pop('code', False)
        username = unidecode(data.pop('username'))
        password = data.pop('password')
        email = unidecode(data.pop('email'))
        # TODO use localization
        birth_date = datetime.strptime(data.pop('birth_date'),
                                       '%d.%m.%Y').strftime('%Y-%m-%d')
        profile = UserProfile.objects.create_user(username, password, email,
                                                  birth_date, **data)

        if code:

            try:
                team_invitation = TeamInvite.objects.get(code=code, user=None)
                team_invitation.user = profile.user
                team_invitation.email = profile.user.email
                team_invitation.save()
                try:
                    ar = AccommodationReservation.objects.get(
                        team=team_invitation.team,
                        user_email=email
                    )
                    ar.user = profile.user
                    ar.save()
                    AccommodationReservation.objects.filter(
                        ~Q(pk=ar.pk) & Q(user_email=email),
                        paymentitem__isnull=True).delete()
                except ObjectDoesNotExist:
                    AccommodationReservation.objects.filter(
                        user_email=email, paymentitem__isnull=True).delete()

                TeamInvite.objects.filter(~Q(pk=team_invitation.pk),
                                          team=team_invitation.team,
                                          email=email).delete()
            except ObjectDoesNotExist:
                messages.add_message(
                    request, messages.INFO,
                    _('Registering on invite failed as someone already'
                      ' registered on that code.'))

        send_activation_mail(profile.user, request)

        messages.add_message(
            request,
            messages.INFO,
            _('After you login, you can accept the invite to the team.'))

        return render(request, 'profiles/post_register.html', {'email': email})


class UserProfileEditorView(SingleObjectMixin, PermissionRequiredMixin, View):
    model = User
    permission_required = 'profiles.change_userprofile'

    def get(self, request, pk):

        user = self.get_object()
        user_form = EditUserForm(instance=user)

        try:
            user_profile = user.profile
            user_profile_form = EditUserProfileForm(instance=user_profile)
            user_profile_form.initial['birth_date'] = (user_profile.birth_date
                                                       .strftime("%d.%m.%Y"))
            user_profile_form.initial['city'] = user_profile.city
            user_profile_form.initial['street'] = user_profile.street
            user_profile_form.initial['zip_code'] = user_profile.zip_code

        except ObjectDoesNotExist:
            user_profile_form = EditUserProfileForm()

        return render(request, 'profiles/edit.html',
                      context={
                          'user': user,
                          'user_form': user_form,
                          'user_profile_form': user_profile_form
                      })

    def post(self, request, pk):
        user = self.get_object()
        user_form = EditUserForm(request.POST)
        user_profile_form = EditUserProfileForm(request.POST)
        if not user_form.is_valid() or not user_profile_form.is_valid():
            return render(request, 'profiles/edit.html',
                          context={
                              'user': user,
                              'user_form': user_form,
                              'user_profile_form': user_profile_form
                          })

        user_data = user_form.cleaned_data
        user.first_name = user_data['first_name']
        user.last_name = user_data['last_name']
        user.save()

        profile_data = user_profile_form.cleaned_data
        UserProfile.objects.update_or_create(
            user=self.get_object(),
            defaults={
                'country': profile_data['country'],
                'state': profile_data['state'],
                'gender': profile_data['gender'],
                'birth_date': profile_data['birth_date'],
                'status': profile_data['status'],
                'school': profile_data['school'],
                'school_year': profile_data['school_year'],
                'phone_number': profile_data['phone_number'],
                'ynet_id': profile_data['ynet_id']}
        )
        messages.success(request, _("Changes saved."))
        return redirect('view_profile', username=self.get_object().username)


class PlayerProfileAddView(SingleObjectMixin, PermissionRequiredMixin, View):

    permission_required = 'profiles.add_playerprofile'
    slug_field = 'username'
    slug_url_kwarg = 'username'
    model = User

    def get(self, request, username):
        form = PlayerProfileForm()
        return render(request, 'player_profile/choose_add.html',
                      context={'form': form})

    def post(self, request, username):
        form = PlayerProfileForm(request.POST)
        if form.is_valid():
            player_profile = None
            try:
                player_profile = PlayerProfile.objects.create(
                    game=form.cleaned_data['game'],
                    user=request.user,
                    nick=form.cleaned_data['nick'])
                return redirect('view_profile', username=username)
            except IntegrityError:
                messages.add_message(
                    request,
                    messages.WARNING,
                    _('You already have a player profile in that game.'
                      ' Try returning to your profile.'))
                return render(request, 'player_profile/choose_add.html',
                              context={'form': form})
            except:
                if player_profile is not None:
                    player_profile.delete()
        return render(request, 'player_profile/choose_add.html',
                      context={'form': form})


class PlayerProfileEditorView(SingleObjectMixin, PermissionRequiredMixin,
                              View):

    permission_required = 'profiles.change_playerprofile'
    slug_url_kwarg = 'username'
    slug_field = 'username'
    model = User

    def get(self, request, username, game):
        user = User.objects.get(username=username)
        game = Game.objects.get(slug=game)
        player_profile = PlayerProfile.objects.get(user=user, game=game)
        form = PlayerProfileForm(instance=player_profile)
        return render(request, 'player_profile/edit.html',
                      context={'form': form})

    def post(self, request, username, game):
        form = PlayerProfileForm(request.POST)
        if form.is_valid():
            try:
                user = User.objects.get(username=username)
                game = Game.objects.get(slug=game)
                pp = PlayerProfile.objects.filter(user=user, game=game)
                pp.update(nick=form.cleaned_data['nick'], approved=False)
                return redirect('view_profile', username=username)
            except:
                pass
        return render(request, 'player_profile/edit.html',
                      context={'form': form})

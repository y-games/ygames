from phonenumber_field.widgets import PhoneNumberPrefixWidget


class BootstrapPhoneNumberWidget(PhoneNumberPrefixWidget):
    def format_output(self, rendered_widgets):
        return ('<div class="row">'
                '<div class="col-xs-5 col-sm-4">%s</div>'
                '<div class="col-xs-7 col-sm-8">%s</div>'
                '</div>') % tuple(rendered_widgets)

    def value_from_datadict(self, data, files, name):
        values = super(PhoneNumberPrefixWidget, self).value_from_datadict(
            data, files, name)
        if all(values):
            return '%s.%s' % tuple(values)
        return ''


def create_valid_response(is_valid, message=None):
    if is_valid:
        return {
            'valid': True,
        }
    return {
        'valid': False,
        'message': message
    }


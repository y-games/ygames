from datetime import datetime

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField

from teritories.models import Territory
from ygames.utils import generate_code

GENDERS = (
    ('m', _('Male')),
    ('f', _('Female')),
)
STATUSES = (
    ('s', _('Student')),
    ('w', _('Employed')),
    ('o', _('Other'))
)


class UserManager(models.Manager):
    def create_user(self, username, password, email, birth_date, **kwargs):
        if 'is_staff' not in kwargs:
            kwargs['is_staff'] = False
        if 'is_active' not in kwargs:
            kwargs['is_active'] = False
        activation_code = generate_code()
        code_expiration = datetime.now() + settings.ACTIVATION_TIME_OFFSET
        user = User.objects.create_user(
            username,
            email,
            password,
            first_name=kwargs['first_name'],
            last_name=kwargs['last_name'],
        )
        user.is_active = False
        user.save()
        try:
            profile = self.create(
                user=user,
                country=kwargs['country'],
                state=kwargs['state'],
                gender=kwargs['gender'],
                birth_date=birth_date,
                status=kwargs['status'],
                school=kwargs['school'],
                school_year=kwargs['school_year'],
                phone_number=kwargs['phone_number'],
                ynet_id=kwargs['ynet_id'],
                code=activation_code,
                code_expiration=code_expiration
            )
        except Exception as e:
            user.delete()
            raise e
        return profile

    def get_by_uid(self, uid):
        return self.get(user__id=uid)

    def get_by_username(self, username):
        return self.get(user__username=username)

    def get_by_email_or_none(self, email):
        try:
            return self.get(user__email=email)
        except ObjectDoesNotExist:
            return None


class UserProfile(models.Model):
    user = models.OneToOneField('auth.User', related_name='profile',
                                null=False)

    country = CountryField()
    state = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=GENDERS)
    birth_date = models.DateField()
    status = models.CharField(max_length=1, choices=STATUSES)
    school = models.CharField(max_length=100, blank=True)
    school_year = models.PositiveIntegerField(null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    street = models.CharField(max_length=100, null=True, blank=True)
    zip_code = models.CharField(max_length=32, null=True, blank=True)

    phone_number = PhoneNumberField(blank=True, null=True)

    ynet_id = models.CharField(max_length=50, blank=True, null=True)

    code = models.CharField(max_length=64, blank=True, db_index=True)
    code_expiration = models.DateField(blank=True, null=True, default=None)

    objects = UserManager()

    def __str__(self):
        return 'Y-{}'.format(self.user)

    def get_state_display(self):
        try:
            territory = Territory.objects.get(code=self.state)
            return territory.name
        except ObjectDoesNotExist:
            return self.state


class PlayerProfile(models.Model):
    game = models.ForeignKey('game.Game', db_index=True,
                             related_name='player_profiles')
    user = models.ForeignKey('auth.User', db_index=True,
                             related_name='player_profiles')

    nick = models.CharField(max_length=100)

    class Meta:
        unique_together = (('game', 'user'), ('game', 'nick'))
        ordering = ['user__username']

    def __str__(self):
        return '{} - [{}] - {}'.format(self.game, self.user, self.nick)

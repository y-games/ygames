TAG_SHADOW_NAMES = {'cls': 'class', 'ID': 'id', 'aria_hidden': 'aria-hidden'}


def tag(tag_name, **attributes):
    attr_str = ' '.join(('{}="{}"'.format(TAG_SHADOW_NAMES.get(k, k), v)
                         for k, v in attributes.items()))

    def tag_deco(func):
        def func_wrapper(*args, **kwargs):
            return '<{0} {1}>{2}</{0}>'.format(tag_name, attr_str,
                                               func(*args, **kwargs))

        return func_wrapper

    return tag_deco

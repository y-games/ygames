"""ygames URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
import django.contrib.auth.views as auth_views
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.base import RedirectView

from profiles.forms import (PassResetForm,
                            SetPassForm)

from . import settings

urlpatterns = [
    url(r'^territories/', include('teritories.urls')),


    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', auth_views.login, {'template_name': 'auth/login.html'},
        name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'auth/logout.html'},
        name='logout'),
    # TODO create app passwords and add only urls in it
    url(r'^password/reset/$', auth_views.password_reset,
        {'template_name': 'auth/password_reset.html',
         'password_reset_form': PassResetForm}, name='password_reset'),
    url(r'^password/reset/done/$', auth_views.password_reset_done,
        {'template_name': 'auth/password_reset_done.html'},
        name='password_reset_done'),
    url(
        r'^password/reset/confirm/(?P<uidb64>[a-zA-Z0-9+/=]+)/(?P<token>[\w-]+)/$',
        auth_views.password_reset_confirm,
        {'template_name': 'auth/password_reset_confirm.html',
         'set_password_form': SetPassForm},
        name='password_reset_confirm'
    ),
    url(r'^password/reset/complete/$', auth_views.password_reset_complete,
        {'template_name': 'auth/password_reset_complete.html'},
        name='password_reset_complete'),
    url(r'^password/change/$', auth_views.password_change,
        {
            'post_change_redirect': 'index',
            'template_name': 'auth/password_change_form.html'
        }, name='password_change'),
    url(r'^password/change/done/$', auth_views.password_change_done,
        name='password_change_done'),

    url(r'^teams/', include('teams.urls')),
    url(r'^profiles/', include('profiles.urls')),
    url(r'^accommodations/', include('accommodations.urls')),
    url(r'^payments/', include('payments.urls')),
    url(r'^game/', include('game.urls')),
    url(r'^tinymce/', include('tinymce.urls')),

    url(r'^favicon.ico$',
        RedirectView.as_view(url=settings.STATIC_URL + 'favicon.ico',
                             permanent=True), name='favicon'),

    url(r'^robots.txt$',
        RedirectView.as_view(url=settings.STATIC_URL + 'robots.txt',
                             permanent=True), name='robots'),
    url(r'^', include('web.urls')),
]

urlpatterns += staticfiles_urlpatterns()

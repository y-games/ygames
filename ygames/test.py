from django.test import TestCase

from ygames.utils import generate_code, generate_vs, create_link


class GenerateCodeTest(TestCase):

    def test_generate_code(self):
        text = generate_code()
        self.assertNotEqual('', text)
        self.assertEqual(32, len(text))


class GenerateVSTest(TestCase):

    def test_generate_vs(self):
        team_id = 1250
        tournament_id = 8
        gamer_rule_id = 89
        code = generate_vs(tournament_id, gamer_rule_id, team_id)
        self.assertEqual('08891250', code)

    def test_overflow_vs(self):
        team_id = 9870
        tournament_id = 15
        gamer_rule_id = 135
        code = generate_vs(tournament_id, gamer_rule_id, team_id)
        self.assertEqual('15359870', code)


class HttpLinkTest(TestCase):

    def test_create_link(self):
        domain = 'knife.is.in.the.kitchen'
        query = (('pos_x', 1), ('pos_y', 12), )
        path = '/path/to/refrigerator'
        # FIXME this test is dependent on position of key in dict this is bad
        authority = 'http://knife.is.in.the.kitchen/path/to/refrigerator/'
        self.assertEqual(authority, create_link(domain, path, query)[:len(authority)])
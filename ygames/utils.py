import random
import string
from datetime import datetime

from django.contrib.sites.shortcuts import get_current_site

# TODO write tests


def generate_code(length=32, charset=string.ascii_letters + string.digits):
    """
    Generate random string of specified length from specified charset
    :param length: length of generated string
    :type length: int
    :param charset: set of characters from witch string would be generated
    :type charset: str
    :return: random-ish string of specified length
    :rtype: str
    """
    return ''.join((random.choice(charset) for _ in range(0, length)))


def is_secure(request):
    """
    Check if request went thought secured http protocol.
    :param request: http[s] request
    :return: True if connection was secure, otherwise False
    :rtype: bool
    """
    return ('HTTP_X_FORWARDED_PROTOCOL' in request.META and
            request.META['HTTP_X_FORWARDED_PROTOCOL'] == 'https')


# TODO remove used only in obsolete tests
def generate_vs(tournament_id, game_rule_id, team_id):
    return '{:02d}{:02d}{:04d}'.format(tournament_id, game_rule_id % 100,
                                       team_id % 10000)


def create_link(domain, path='', query=None, scheme='http'):
    # FIXME use :meth:`urljoin` instead
    link = '%s://%s' % (scheme, domain)
    link = link.rstrip('/') + '/'
    if path:
        path = path.strip('/') + '/'
        link += path
    if query:
        query = dict(query)
        link = '%s?%s' % (link,
                          '&'.join('%s=%s' % (k, v) for k, v in query.items()))
    return link


def create_local_link(request, path='', query=()):
    """
    Convenience function that creates link pointing on current site.
    :param request: django request object
    :param path: path to resource
    :param query: key, value query pairs
    :return: url string
    """
    domain = get_current_site(request).domain
    return create_link(domain, path, query, request.scheme)


def span_render(bootstrap_class):
    return '<span class="glyphicon glyphicon-{}"></span>'.format(
        bootstrap_class)


def utc_now():
    """
    Convenience function that creates utc datetime
    :return:
    """
    return datetime.utcnow()


def create_dict(pairs):
    """Creates dictionary from iterable of `(key, value)` pair tuples.

    If multiple tuples has the same key value, their values woule be add in
    list.

    Args:
        pairs (iterable of tuples): Iterable of pair tuples.

    Returns:
        dict: Dictionary from pair tuples.
    """

    if not pairs:
        return {}
    dct = {}
    for key, value in pairs:
        if key in dct:
            if type(dct[key]) is not list:
                dct[key] = [dct[key]]
            dct[key].append(value)
        else:
            dct[key] = value
    return dct


class PathNotFound(Exception):
    pass


def pget(tree, path, **kwargs):
    """Gets value from path.
    If default is set and path is not found in particular tree default value
    is returned otherwise PathNotFoundException is raised.

    Arguments:
        tree (dict|list): Tree (from dicts or lists) with key -> value binding.
        path (str): Path from which values should be returned.
        default (object, optional): Default value that will be returned.

    Returns:
        Value from tree that is assigned to certain path. If path is not found
        default option will be returned instead or exception will be raised.

    Raises:
        PathNotFound: Path not found exception, if path is not found and
            default value is not set.
    """
    raise_exc = 'default' not in kwargs
    default = kwargs.get('default', None)
    nodes = path.strip().split('.')

    def _in(subtree, node):
        if type(subtree) in (list, tuple) and node.isdecimal():
            return 0 <= int(node) <= len(subtree)
        else:
            return node in subtree

    def _get(subtree, node):
        if type(subtree) in (list, tuple) and node.isdecimal():
            return subtree[int(node)]
        else:
            return subtree[node]

    subtree = tree
    for node in nodes:
        if _in(subtree, node):
            subtree = _get(subtree, node)
        elif raise_exc:
            raise PathNotFound("Path %s is not contained in %s" % (path,
                                                                   repr(tree)))
        else:
            return default

    return subtree

"""Ygames settings module."""
import os
import json
from os.path import (expanduser,
                     join,
                     exists)
from datetime import timedelta
from django.utils.translation import ugettext_lazy as _

from .utils import pget

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'a'

DEBUG = True

ALLOWED_HOSTS = ['localhost:8000', 'localhost', 'reg.localhost', '127.0.0.1']

GRAPH_MODELS = {
    'all_applications': True,
    'group_models': True
}

INSTALLED_APPS = (
    'dal',
    'dal_select2',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django_extensions',
    'rules.apps.AutodiscoverRulesConfig',
    'tinymce',
    'bootstrap3',
    'subdomains',
    'django_countries',
    'crispy_forms',
    'django_ajax',
    'teritories',
    'game',
    'profiles',
    'accommodations',
    'teams',
    'web',
    'include',
    'payments',
    'django_tables2',
    'daterange_filter',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'a',
        'USER': 'a',
        'PASSWORD': 'a',
        'HOST': 'localhost',
        'PORT': '',
    }
}

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'rules.permissions.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

CLASSIFIER = {
    '': {},
    'alert': {
        '': 'alert-info',
        'error': 'alert-danger',
        'info': 'alert-info',
        'warning': 'alert-info',
        'debug': 'alert-info',
        'success': 'alert-success'
    }
}

ROOT_URLCONF = 'ygames.urls'

SUBDOMAIN_URLCONFS = {
    None: 'ygames.urls',
    'www': 'ygames.urls',
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
            ],
        },
    },
]

WSGI_APPLICATION = 'ygames.wsgi.application'

AUTH_PROFILE_MODULE = 'profiles.UserProfile'

COUNTRIES_FIRST = ['SK', 'CZ', 'AT', 'HU']

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'sk'

LANGUAGES = (
    ('sk', _('Slovak')),
    ('cs', _('Czech')),
    ('de', _('German')),
    ('hu', _('Hungarian')),
    # ('en', _('English')),  # TODO enable this when translations are made
)

LANGUAGE_COOKIE_NAME = 'ygames_lang'

LOCALE_PATHS = [os.path.join(BASE_DIR, 'locale')]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

LOGIN_REDIRECT_URL = '/'

SITE_ID = 2

ACTIVATION_TIME_OFFSET = timedelta(days=14)

CRISPY_TEMPLATE_PACK = 'bootstrap3'

LOGIN_URL = '/login/'

ADMINS = []

MANAGERS = []

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': ('\n-----\n%(levelname)s %(asctime)s %(pathname)s'
                       ' %(process)d %(thread)d\n %(message)s\n-----\n')
        },
        'simple': {
            'format': '%(asctime)s %(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True
        },
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = None
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
DEFAULT_FROM_EMAIL = ''
SERVER_EMAIL = ''
EMAIL_FILE_PATH = '/tmp/ygames/mails/'

TINYMCE_DEFAULT_CONFIG = {
    'theme': 'advanced',
    'plugins': ('advimage,advlink,autolink,autosave,media,searchreplace,paste,'
                'preview,autoresize'),
    'theme_advanced_buttons1_add': "media",
    'theme_advanced_buttons3_add': "preview",
    'plugin_preview_width': "500",
    'plugin_preview_height': "600"
}

BTICKET_EMAIL = 'somemail@test.sk'

# Settings patching ===========================================================
_ALLOWED = (int, float, str, list, type(None), bool, dict)

_SETTINGS_PATH = (os.environ["SETTINGS_PATH"]
                  if 'SETTINGS_PATH' in os.environ else 'YGames/settings.json')


def _get_global_settings():
    """Returns list of global settings.
    #
    Settings must not start with _, must be SCREAMING_SNAKE_CASED and their
    value type must by one of :const:`_ALLOWED`.

    Returns:
        (list of str): list of setting names.
    """
    _allowed = set(_ALLOWED)
    return {key for key, value in globals().items()
            if all((not key.startswith('_'),
                    key.upper() == key,
                    type(value) in _allowed))}


def _substitute_globals(config):
    """Substitues global variables with those in :var:`config` dict.

    Arguments:
        config (dict): Configuration dictionary with substitutional values.
    """
    if not isinstance(config, dict):
        raise ValueError("Configuration is not instance of type dict.")

    constants = _get_global_settings()
    _allowed = set(_ALLOWED)
    for key, value in config.items():
        if key in constants and type(value) in _allowed:
            globals()[key] = value


def _load_settings():
    """Loads settings from most specific path.

    Paths are checked in this succession (`$HOME/YGames/settings.json`,
    `/etc/YGames/settings.json`).
    """
    home_path = join(expanduser("~"), _SETTINGS_PATH)
    etc_path = join('/etc', _SETTINGS_PATH)
    read_path = None
    if exists(home_path):
        read_path = home_path
    elif exists(etc_path):
        read_path = etc_path

    if not read_path:
        return "{}"

    with open(read_path) as settings_file:
        lines = settings_file.read()
        print(lines)
        return lines


def _assert_constraints():
    def _format_error(var_name, msg):
        msg = repr(msg) if msg else "UNSET!"
        return "You have to set {} ({}) in {} configuration file".format(
            var_name, msg, _SETTINGS_PATH)

    def _is_not_empty(var_name):
        assert globals()[var_name] not in ('', None, [], {}), _format_error(
            var_name,
            globals()[var_name]
        )

    def _inner_is_not_empty(path):
        assert pget(globals(), path, default='') not in ('', None, [], {}), \
            _format_error(
                path,
                pget(globals(), path, default='')
            )

    _is_not_empty('SECRET_KEY')
    _is_not_empty('ADMINS')
    _is_not_empty('MANAGERS')
    _inner_is_not_empty('DATABASES.default.NAME')
    _inner_is_not_empty('DATABASES.default.PASSWORD')
    _inner_is_not_empty('DATABASES.default.USER')


def _apply_settings():
    """Applies settings from `*/_SETTINGS_PATH` file."""
    _substitute_globals(json.loads(_load_settings()))
    if not os.environ.get("READTHEDOCS"):
        _assert_constraints()


_apply_settings()

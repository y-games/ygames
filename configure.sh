#!/bin/sh

PROJECT="ygames"
DATABASE="ygames"
PGPASS="password"
PGHOST="localhost"
PGUSER="$USER"
PGPORT=5432
WORKING_DIR=`pwd`
BACKUP_DIR=/var/pgbackups/

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -d|--database)
    PGDATABASE="$2"
    shift # past argument
    ;;
    -h|--host)
    PGHOST="$2"
    shift # past argument
    ;;
    -U|--user)
    PGUSER="$2"
    shift # past argument
    ;;
    -W|--password)
    PGPASS="$2"
    ;;
    -p|--port)
    PGPORT="$2"
    shift
    ;;
    -b|--backup-dir)
    BACKUP_DIT="$2"
    shift
    ;;
    *)
    echo "UNKNOWN OPTION $key" >&2
    exit 1
    ;;
esac
shift # past argument or value
done

if [ ! -f ./ygames/wsgi.py ]; then
    echo "Script called from wrong directory"
    exit 1
fi

mkdir -p ${BACKUP_DIR}

if [ -f $HOME/pgbackup.sh ]; then
    cat $HOME/pgbackup.sh | sed "s/^DATABASES=\"\([^\"]*\)/DATABASES=\"\1 ${DATABASE}\"/" > $HOME/pgbackup.sh
fi

if [ ! -f $HOME/.pgpass ]; then
cat > $HOME/.pgpass << EOF
${PGHOST}:${PGPORT}:${DATABASE}:${PGUSER}:${PGPASS}
EOF
else
    cat $HOME/.pgpass | sed "/:${DATABASE}:/" > $HOME/.pgpass
    echo ":${PGHOST}:${PGPORT}:${DATABASE}:${PGUSER}:" >> $HOME/.pgpass
fi

chmod $HOME/.pgpass

if [ ! -f $HOME/pgbackup.sh ]; then
    echo "Creating backup script and cron-jobs."

cat > $HOME/pgbackup.sh << EOF
#!/bin/sh
DATABASES="ygames ygames_beta"
BACKUP_DIR=${BACKUP_DIR}
LOGFILE=/var/log/\$0

for db in \$DATABASES
do
    echo "Dumping \${db}" >> \$LOGFILE
    pg_dump -E UTF-8 -F c -f "\$BACKUP_DIR/\${db}_\$(date +%F_%R).tar.bz2" \$db
done

exit 0
EOF

    chmod +x $HOME/pgbackup.sh

cat > $HOME/housekeeping.sh << EOF
#!/bin/sh
BACKUP_DIR=${BACKUP_DIR}
LOGFILE=/var/log/\$0

rm -f \$LOGFILE
for file in `find \$BACKUP_DIR -mtime +30 -type f -name *.tar.bz2`
do
    echo "Deleting: \${file}" >> \$LOGFILE
    rm \$file
done

exit 0
EOF

    chmod +x $HOME/housekeeping.sh

    crontab -l > /tmp/crontab
    sed -i '/pgbackup.sh/' /tmp/crontab
    sed -i '/housekeeping.sh/' /tmp/crontab
    echo "0 1 * * * $HOME/pgbackup.sh" >> /tmp/crontab
    echo "0 2 * * 1 $HOME/housekeeping.sh" >> /tmp/crontab
    crontab /tmp/crontab
    rm -f /tmp/crontab
fi

if [ ! -f /etc/nginx/sites-available/${PROJECT}.conf ]; then
    echo "Creating nginx config file."
cat > /etc/nginx/sites-available/${PROJECT}.conf << EOF
server {
    listen 80;
    server_name localhost;
    document_root ${WORKING_DIR};

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        alias ${WORKING_DIR}/staticfiles/;
    }

    location / {
        if (-f \$document_root/maintenance.html) {
            return 503;
        }
        proxy_set_header HOST \$http_host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_pass http://unix:${WORKING_DIR}/${PROJECT}.sock;
    }

    error_page 503 @maintenance;
    location @maintenance {
        rewrite ^(.*)$ /maintenance.html break;
    }
}
EOF
fi

if [ ! -f /lib/systemd/system/ygames.service ]; then
    echo "Creating systemd service conf."
    mkdir -p /lib/systemd/system
cat > /lib/systemd/system/ygames.service << EOF
[Unit]
Description=YGames gunicorn service
After=network.target

[Service]
User=${USER}
Group=www-data
WorkingDirectory=${WORKING_DIR}
ExecStart=${WORKING_DIR}/env/bin/gunicorn --workers 3 --bind unix:${WORKING_DIR}/ygames.sock ygames.wsgi:application
ExecStop=/bin/kill -s TERM \$MAINPID
Restart=always

[Install]
WantedBy=multi-user.target
EOF
fi

if [ ! -f /etc/YGames/settings_default.json ]; then
    echo "Creating settings file in /etc/YGames/"
    mkdir -p /etc/YGames/

cat > /etc/YGames/settings_default.json << EOF
{
  "ALLOWED_HOSTS": ["localhost"],
  "SECRET_KEY": "",
  "DATABASE": {
    "default": {
      "ENGINE": "django.db.backends.postgresql_psycopg2",
      "HOST": "${PGHOST}",
      "PORT": "",
      "USER": "${PGUSER}",
      "DATABASE": "${DATABASE}",
      "PASSWORD": "${PGPASS}"
    }
  },
  "EMAIL_BACKEND": "django.core.mail.backends.smtp.EmailBackend",
  "EMAIL_HOST": "",
  "EMAIL_PORT": 25,
  "EMAIL_HOST_USER": "",
  "EMAIL_HOST_PASSWORD": "",
  "SERVER_EMAIL": "",
  "ADMINS": [],
  "MANAGERS": []
}
EOF
fi


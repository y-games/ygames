from datetime import datetime

from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.views.decorators.http import require_GET

from game.models import Tournament, Game, GameRules


@require_GET
def show_game(request, slug):
    game = get_object_or_404(Game, slug=slug)
    with_enabled_reg = Tournament.objects.filter_enabled_registration()
    game_rules = GameRules.objects.filter(game=game,
                                          tournament__in=with_enabled_reg)
    context = {'game': game, 'game_rules': game_rules}
    return render(request, 'game/game_detail.html', context=context)


@require_GET
def show_games(request):
    now = datetime.now()
    games = Game.objects.filter(
        gamerules__tournament__registration_start__lte=now,
        gamerules__tournament__registration_end__gte=now
    ).distinct('pk')
    print(len(games))
    return render(request, 'game/games.html', context={'games': games})
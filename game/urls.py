from django.conf.urls import url

from game import views

urlpatterns = [
    url('^$', views.show_games, name='show_games'),
    url('^(?P<slug>[A-Za-z0-9_.-]+)/$', views.show_game, name='show_game'),
]

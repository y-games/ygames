from django.contrib import admin
from . import models


class GameAdmin(admin.ModelAdmin):
    list_display = ('name', 'acronym', 'slug')


admin.site.register(models.Game, GameAdmin)


class TournamentAdmin(admin.ModelAdmin):
    list_display = ('name', 'acronym', 'slug')


admin.site.register(models.Tournament, TournamentAdmin)


class GameRulesAdmin(admin.ModelAdmin):
    list_display = ('name', 'game', 'tournament', 'capacity',
                    'player_count', 'teams_left')
    list_filter = ('game', 'tournament', 'capacity', 'player_count')


admin.site.register(models.GameRules, GameRulesAdmin)



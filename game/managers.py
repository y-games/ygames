from django.db import models
from datetime import datetime


class TournamentManager(models.Manager):

    def exist_enabled_registration(self):
        return self.filter_enabled_registration().exists()

    def filter_enabled_registration(self):
        return self.filter(registration_start__lte=datetime.now(),
                           registration_end__gte=datetime.now())

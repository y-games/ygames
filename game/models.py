from datetime import datetime

from autoslug import AutoSlugField
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from tinymce.models import HTMLField

from game.managers import TournamentManager
from teams.models import Team


def acronym_first(instance):
    return instance.acronym or instance.name

# TODO create proxy for models


class AcronymMixin:

    def __str__(self):
        if getattr(self, 'acronym', ''):
            return '[{}] {}'.format(getattr(self, 'acronym'),
                                    getattr(self, 'name'))
        else:
            return str(getattr(self, 'name'))


class Game(AcronymMixin, models.Model):
    name = models.CharField(max_length=50, unique=True)
    acronym = models.CharField(max_length=10, blank=True)
    slug = AutoSlugField(populate_from=acronym_first, unique=True)
    info = HTMLField(blank=True)
    name_for_nick = models.CharField(max_length=500, default='Nick')

    def acronym_nospace(self):
        return self.acronym.replace(" ", "")

    def __str__(self):
        return '[{}] {}'.format(self.acronym, self.name)


class Tournament(AcronymMixin, models.Model):
    name = models.CharField(max_length=255, unique=True)
    acronym = models.CharField(max_length=50, blank=True)
    slug = AutoSlugField(populate_from=acronym_first, unique=True)
    info = HTMLField(blank=True)
    gateway_url = models.URLField(default='', blank=True)
    event_admin_email = models.EmailField(default='', blank=True)

    date = models.DateField()

    # TODO prizes to win
    # TODO location
    # TODO stanky
    # TODO partners
    # TODO price of tickets

    registration_start = models.DateField()
    registration_end = models.DateField()

    games = models.ManyToManyField(Game, through='GameRules')

    objects = TournamentManager()

    def __str__(self):
        if self.acronym:
            return '[{}] {}'.format(self.acronym, self.name)
        else:
            return self.name

    def is_registration_active(self):
        return (self.registration_start <= datetime.date(datetime.now()) <=
                self.registration_end)


class GameRulesManager(models.Manager):

    def get_teams_left(self, game_rule):
        if not isinstance(game_rule, GameRules):
            if type(game_rule) is str:
                game_rule = self.get(slug=game_rule)
            elif type(game_rule) is int:
                game_rule = self.get(pk=game_rule)
            else:
                raise ObjectDoesNotExist
        registered = game_rule.team_set.count()
        return game_rule.capacity - registered


def concat_slug(instance):
    return ' '.join(filter(None, (instance.tournament.slug,
                                  instance.game.slug,
                                  instance.name)))


class GameRules(models.Model):

    game = models.ForeignKey(Game)
    tournament = models.ForeignKey(Tournament)
    name = models.CharField(max_length=64, db_index=True, blank=True)
    vip = models.BooleanField(default=False)

    capacity = models.IntegerField()
    specific_rules = HTMLField()
    player_count = models.PositiveIntegerField()

    price = models.IntegerField()
    price_per_player = models.IntegerField()
    ticket_id = models.IntegerField(null=False, db_index=True, default=-1)
    slug = AutoSlugField(populate_from=concat_slug, unique=True)

    objects = GameRulesManager()

    class Meta:
        verbose_name_plural = 'Game Rules'

    def __str__(self):
        if self.vip:
            return '{} - {} - {} [VIP]'.format(self.tournament.acronym,
                                               self.game.name, self.name)
        else:
            return '{} - {} - {}'.format(self.tournament.acronym,
                                         self.game.name, self.name)

    def get_game_name(self):
        return '{} ({})'.format(self.game.name, self.name)

    @property
    def teams_left(self):
        return GameRules.objects.get_teams_left(self)


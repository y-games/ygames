Installation instructions
=========================

Setup database
--------------

Download and install postgresql (Ubuntu 16.04).

.. code:: sh

    $ apt update && apt install postgresql

Create database and setup new role that have privileges to write into that database.

.. code:: sh

    $ su - postgre

.. code:: sh

    $ psql

.. code:: psql

    CREATE DATABASE [DATABASENAME];
    CREATE USER [USER] WITH PASSWORD '[PASSWORD]';
    GRANT ALL PRIVILEGES ON DATABASE [DATABASENAME] TO [USER];
    \q

Install Nginx
-------------

Install nginx server from repository.

.. code:: sh

    $ apt install nginx

Install YGames portal
---------------------

Download ygames app unpack it if it's packaged. And run project installation script.

.. code:: sh

    $ cd /path/to/project

.. code:: sh

    $ ./configure.sh --project ygames --database ygames -u USER -w PASSWORD

This script should create/update few scripts on your machine. Most interesting
for us is settings config in */etc/YGames/settings_default.json*

.. code:: sh

    $ cp /etc/YGames/settings_default.json /etc/YGames/settings.json

And eddit it according to your needs.

.. code:: sh

    $ vim /etc/YGames/settings.json

With this file you could patch every setting that is contained in settings.py file.
Although some restrictions are made, like setting must be SCREAMING_SNAKE_CASED and
it's values must be json compliant types. There are few settings that *must be* set
like SECRET_KEY, DATABASE, ADMINIS, MANAGERS. For more information look in :file:`ygames/settings.py`
file in :func:`ygames.settings._asert_constraints`.

.. warning:: Not all settings should be patched!!

    Some settings use function closures, which can not be
    unmarshalled now (and probably this wouldn't be possible
    in the future either). Generally the rule of thumb is that
    every setting that consists only from json compliant types
    (bool, num, null, string, list, dictionary) is patchable.

After configuration of project settings we should be able to run :file:`./update.sh` script.
Which will automatically update/install project on your server.

.. code:: sh

    $ ./update.sh

This time it should create database and startup gunicorn processes, that are connected with nginx
via unix socket in project directory. Stop the application for now and create superuser.

.. code:: sh

    $ systemctl stop ygames  # assuming we named our project ygames

.. code:: sh

    $ source env/bin/activate
    $ python ./manage.py createsuperuser

This application will ask for credentials and some other information.

Start the project once again.

.. code:: sh

    $ systemctl start ygames

Now configure your nginx projec.conf scrip.

.. code:: sh

    $ vim /etc/nginx/sites-available/ygames.conf

Most likely you just want to change line with server_name line to something like::

    server_name ygames.sk y-games.sk registracie.ygames.sk registracie.y-games.sk;

Now enable that site config by symlinking it to sites-enabled.

.. code:: sh
    $ ln -s /etc/nginx/sites-available/ygames.conf /etc/nginx/sites-enabled/

And reload your nginx service.

.. code:: sh

    $ systemctl reload nginx

Now the site should be up and running. You might check it using your web browser.

Upgrade
=======

Download new version of portal. Unpack it or whatever and run installation script.
Entire upgrade should be as easy as running one script.

.. code:: sh

    $ ./update.sh

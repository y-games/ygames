{% load i18n %}
{% trans "Hi" %} {{ username }} .

{% trans "You received this email, because you created account on" %} {{ request.scheme }}://{{ request.get_host }}{% url 'index' %}
{% trans "Your account can be activeated using following link" %}. {{ request.scheme }}://{{ request.get_host }}{% url 'activate' username code %}

{% trans "Y-Games team" %}
from django.http import HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
from django_ajax.decorators import ajax

from teritories.models import Territory


@csrf_exempt
@ajax
def load_territories(request):
    if request.method != 'POST':
        raise HttpResponseNotAllowed(['POST'])
    country_code = request.POST['value']
    territories = Territory.objects.filter(country=country_code)
    territory_list = []
    for territory in territories:
        territory_list.append({
            "text": territory.name,
            "value": territory.code
        })
    return territory_list

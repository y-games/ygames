from django.db import models

from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField


class Territory(models.Model):

    country = CountryField(blank=False, db_index=True)
    name = models.CharField(max_length=255, blank=False)
    code = models.CharField(max_length=32, blank=False, unique=True, db_index=True)

    class Meta:
        verbose_name = _('Region')
        verbose_name_plural = _('Regions')

/**
 * Created by filip on 19.8.2016.
 */

(function ($) {
    $.fn.replaceWithInput = function (inputClass, value) {
        var _name = $(this).attr('name');
        var _id = $(this).attr('id');
        var _url = $(this).attr('ajax-url');
        var $input = $('<input type="text">').attr('name', _name).attr('id', _id).attr('ajax-url', _url);
        $input.val(value);
        if (inputClass != null) {
            $input.attr('class', inputClass);
        }
        $(this).replaceWith($input);
        return $input;
    };
    $.fn.replaceWithSelect = function (options, value, selectClass) {
        var _name = $(this).attr('name');
        var _id = $(this).attr('id');
        var _url = $(this).attr('ajax-url');
        var $select = $('<select></select>').attr('id', _id).attr('ajax-url', _url).attr('name', _name);
        if (selectClass != null) {
            $select.attr('class', selectClass);
        }
        for (var i = 0; i < options.length; i++) {
            var option = options[i];
            var $optionElement = $("<option></option>").attr('value', option.value).text(option.text);
            if (option.value == value) {
                $optionElement.attr('selected', 'selected');
            }
            $select.append($optionElement);
        }
        $(this).replaceWith($select);
        return $select;
    };
    $.fn.bindSelect = function ($element, options) {
        if ($element == undefined || $element.attr('ajax-url') == '') {
            return;
        }
        var _url = $element.attr('ajax-url');
        var _class = $element.attr('class') || '';
        var _defaultValue = $element.val();
        var _options = {
            selectClass: _class,
            inputClass: _class
        };
        if (options != undefined) {
            if (options.class !== undefined) {
                _options.selectClass = options.class;
                _options.inputClass = options.class;
            }
            if (options.selectClass !== undefined) {
                _options.selectClass = options.selectClass;
            }
            if (options.inputClass !== undefined) {
                _options.inputClass = options.inputClass;
            }
        }
        var $observable = $(this);
        var _lazyCache = {};
        var handlerProvider = function ($element) {
            return function () {
                var val = $observable.val();

                if (val in _lazyCache) {
                    if (_lazyCache[val] != null && _lazyCache[val].length != 0) {
                        $element = $element.replaceWithSelect(_lazyCache[val], _defaultValue, _options.selectClass);
                    } else {
                        $element = $element.replaceWithInput(_options.inputClass, _defaultValue);
                    }
                    $observable.off('change');
                    $observable.on('change', handlerProvider($element));
                } else {
                    $.ajax({
                        url: _url,
                        method: 'POST',
                        data: {value: val},
                        success: function (data) {
                            if (data == null || data.content == null || !data.content.length) {
                                $element = $element.replaceWithInput(_options.inputClass, _defaultValue);
                                _lazyCache[val] = [];
                            } else {
                                $element = $element.replaceWithSelect(data.content, _defaultValue, _options.selectClass);
                                _lazyCache[val] = data.content;
                            }
                            $observable.off('change');
                            $observable.on("change", handlerProvider($element));
                        },
                        error: function () {
                            $element = $element.replaceWithInput(_options.inputClass, _defaultValue);
                            $observable.off('change');
                            $observable.on("change", handlerProvider($element));
                        }
                    });
                }
            };
        };
        $(this).on('change', handlerProvider($element));
        $(document).ready(handlerProvider($element));
    }
})(jQuery);

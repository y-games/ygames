from django.conf.urls import url

from teritories import ajax

urlpatterns = [
    url(r'load/$', view=ajax.load_territories, name='activate'),
]
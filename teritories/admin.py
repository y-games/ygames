from django.contrib import admin
from teritories import models

# Register your models here.


class ProvinceAdmin(admin.ModelAdmin):
    list_display = ('country', 'name', 'code')
    list_filter = ('country', 'name')

admin.site.register(models.Territory, ProvinceAdmin)
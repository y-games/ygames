#!/usr/bin/env bash

echo "DO NOT USE THIS SCRIPT [NOT-TESTED]"
exit 1

echo "Do you really want to remove ygames installation [yN]"
read answer
if [ ${answer} -ne "y" -o ${answer} -ne "Y" ]; then
    echo "Stopping uninstall"
    exit 0
fi

WORKING_DIR=`pwd`

if [ -f /lib/systemd/system/ygames.service ]; then
    echo "Stopping ygames service."
    systemctl stop ygames ||:
    systemctl disable ygames
    rm -f /lib/systemd/system/ygames.service
fi

echo "Removing nginx configs."
if [ -f /etc/nginx/sites-enabled/ygames_new.conf ]; then
    rm -f /etc/nginx/sites-enabled/ygames_new.conf
    systemctl reload nginx
fi

rm -f /etc/nginx/sites-available/ygames_new.conf

if [ -f $HOME/pgbackup.sh ]; then
    echo "Making last backup."
    $HOME/pgbackup.sh
    echo "Removing backup scripts."
    rm -f $HOME/pgbackup.sh
    rm -f $HOME/housekeeping.sh
    echo "Removing backup scripts call from cron jobs."
    crontab -l > /tmp/crontab
    sed -i '/pgbackup.sh/' /tmp/crontab
    sed -i '/housekeeping.sh/' /tmp/crontab
    crontab /tmp/crontab
    rm -f /tmp/crontab
fi

cd /
echo "Removing ygames installation."
rm -Rf ${WORKING_DIR}

exit 0